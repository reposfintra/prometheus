<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>

<html>
<head>
  <title>Listado de Remisiones</title>
  <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
  <SCRIPT language=JavaScript1.2 
    src="<%=BASEURL%>/js/coolmenus3.js">
  </SCRIPT>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
 <BR> 
 <center>    
  
 <%
   Planillas  planilla = model.PlanillasSvc.getPlanilla() ;
   if (request.getParameter("comentario").equals("") && planilla!=null ){%>
   
  <TABLE width='100%' align='center' class='comentario'><TR><TD ALIGN='center'><FIELDSET><legend class="letraresaltada"><b>NOTA : </b></legend>
          <span class="informacion">Para ver información acerca de <b>Movimientos de anticipos  </b> y <b>Tiempos de viajes</b>, deberá
     hacer Click sobre ellos</span>.
  </FIELDSET></TD></TR></TABLE><br>
  <table width="100%"  border="2">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="50%" class="subtitulo1">Informaci&oacute;n de la Planilla </td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> </td>
        </tr>
      </table>      <TABLE width='100%' align='center' class='tablaInferior' cellspacing='0' cellspading='0'>
        <TR>
          <TH colspan='2' class='fila'> <BR>
            <span class="letraresaltada">PLANILLA &nbsp <%= planilla.getPlanilla() %></span> <BR>
          <BR></TH>
        </TR>
        <TR VALIGN='top'>
          <TD width='50%' class="fila"><BR>
              <BR>
              <TABLE  class='comentario' width='90%' align='right'  cellpadding='0' cellspacing='0'>
                <TR valign='top'>
                  <TD COLSPAN='2' class='comentario2'> <span class="letraresaltada">CARGA Y RUTA</b></span><BR>
                        <BR>
                  </b></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"><b>&nbsp&nbsp&nbsp DISTRITO </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getDistrito() %> <BR>
                  <BR>                  </TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"><b>&nbsp&nbsp&nbsp RUTA </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getOrigen() %> --><%= planilla.getDestino()%> <BR>
                  <BR></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"><b>&nbsp&nbsp&nbsp CARGA </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getCarga() %> &nbsp&nbsp <%= planilla.getUnidad() %> <BR>
                  <BR></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' valign='top' class="letraresaltada"><b>&nbsp&nbsp&nbsp TIPO </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getTipo() %> <BR>
                  <BR></TD>
                </TR>
            </TABLE></TD>
          <TD width='50%' class="fila"><BR>
              <BR>
              <TABLE    class='fondotabla' width='90%' align='left' cellpadding='0' cellspacing='0'>
                <TR valign='top'>
                  <TD COLSPAN='2' class='comentario2'> <span class="letraresaltada">REMISION<BR>
                        </b></span><BR>
                  </b></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp NUMERO : </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getRemision() %> <BR>
                  <BR></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp STDJOB: </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getSJ()     %> &nbsp&nbsp- <%=  planilla.getSJDescripcion() %> <BR>
                  <BR></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"><b>&nbsp&nbsp&nbsp CLIENTE: </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getCliente()%> <br>
                      <%= planilla.getNitCliente() %> <BR>
                  <BR></TD>
                </TR>
            </TABLE></TD>
        </TR>
        <TR VALIGN='top'>
          <TD width='50%' class="fila"><br>
              <br>
              <TABLE    class='fondotabla' width='90%' align='right'  cellpadding='0' cellspacing='0'>
                <TR valign='top'>
                  <TD COLSPAN='2' class='comentario2'> <span class="letraresaltada">VEHICULO </b></span><BR>
                        <BR>
                  </b></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"><b>&nbsp&nbsp&nbsp PLACA </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getPlaca()       %> <BR>
                  <BR></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"><b>&nbsp&nbsp&nbsp CONDUCTOR </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getConductor()   %><br>
                      <%= planilla.getCedulaConductor()   %> <BR>
                  <BR></TD>
                </TR>
                <TR valign='top' width='50%'>
                  <TD class="letraresaltada"><b>&nbsp&nbsp&nbsp PROPIETARIO </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getPropietario() %><br>
                      <%= planilla.getCedulaPropietario() %> <BR>
                  <BR></TD>
                </TR>
            </TABLE></TD>
          <TD width='50%' class="fila"><br>
              <br>
              <!--  LOS MOVIMIENTOS DE ANTICIPOS -->
              <%
              List listaMov = planilla.getListaMovimiento();
              String headMov=   "<HTML><head><title>Movimiento de Anticipos</title>" +
								"<link href=css/estilostsp.css rel=stylesheet>" +
								"<script type=text/javascript src=" + BASEURL + "/js/boton.js></script>" +
								"</head>" +
								"<BODY>" +
								"<table width=100%  border=2>" +
								"  <tr>" +
								"    <td><table width=100% class=tablaInferior>" +
								"      <tr>" +
								"        <td width=50% class=subtitulo1>Movimientos de Anticipos </td>" +
								"        <td width=50% class=barratitulo><img src=" + BASEURL + "/images/titulo.gif width=32 height=20> </td>" +
								"      </tr>" +
								"    </table><TABLE width=100% border=1 bordercolor=#999999 bgcolor=#F7F5F4  class=tablaInferior cellspading=0>" +
								"      <TR class=tblTitulo>" +
								"        <TH>AGENCIA</TH>" +
								"        <TH>TIPO DOCUMENTO</TH>" +
								"        <TH>EGRESO</TH>" +
								"        <TH>ITEM</TH>" +
								"        <TH>CONCEPTO</TH>" +
								"        <TH>VALOR</TH>" +
								"        <TH>MONEDA</TH>" +
								"        <TH>BANCO</TH>" +
								"        <TH>SUCURSAL</TH>" +
								"        <TH>CUENTA</TH>" +
								"      </TR>";							   
              String bodyMov="<TR><TD colspan=10 align=center class=bordereporte><br><br><br><span class=informacion>No tiene movimientos asignados</span><br><br></TD></TR>";   
			  if(listaMov!=null || listaMov.size()>0){
                   Iterator itt=listaMov.iterator();
                   bodyMov="";
				   int i = 0;
                   while(itt.hasNext()){
                       AnticipoPlanilla  mov = (AnticipoPlanilla) itt.next();
					   String clase = (i % 2 == 0 )? "filagris" : "filaazul";
                       bodyMov+="<TR class=" + clase + " ><TD class=bordereporte>"+ mov.getAgencia() +"</TD><TD align=center class=bordereporte>"+ mov.getTipoDocumento()+"</TD><TD align=center class=bordereporte>"+ mov.getDocumento()+"</TD><TD align=center class=bordereporte>"+ mov.getItem() +"</TD><TD align=center class=bordereporte>"+ mov.getConcepto() +"</TD><TD align=right class=bordereporte>"+ mov.getValor() +"</TD><TD align=center class=bordereporte>"+ mov.getMoneda() +"</TD><TD class=bordereporte>&nbsp"+ mov.getBanco() +"</TD><TD class=bordereporte>&nbsp"+ mov.getAgenciaBanco() +"</TD><TD class=bordereporte>&nbsp"+ mov.getCuenta()+"</TD></TR>";  
					   i++;
                    }                     
              }
              String footerMov= "    </TABLE></td>" +
								"  </tr>" +
								"</table>" +
								"<br>" +
								"<center>" +
								"<BR>" +
								"<table width=100% border=0 cellspacing=0 cellpadding=0 align=center>" +
								"  <tr align=left>" +
								"    <td><img src=" + BASEURL  + "/images/botones/regresar.gif name=c_regresar id=c_regresar onMouseOver=botonOver(this); onMouseOut=botonOut(this); onClick=window.close()>" + 
								"  </tr>" +
								"</table>" +
								"</BODY></HTML>";			  
              String vistaMov= headMov + bodyMov + footerMov;
              %>
              <!--  LOS TIEMPOS DE VIAJES ASOCIADOS -->
              <%
               List listaTiempo = planilla.getListaTiempo();
               String headTi=   "<HTML><head><title>Tiempos de Viajes</title>" +
								"<link href=css/estilostsp.css rel=stylesheet>" +
								"<script type=text/javascript src=" + BASEURL + "/js/boton.js></script>" +
								"</head>" +
								"<BODY>" +
								"<table width=800  border=2 align=center>" +
								"  <tr>" +
								"    <td><table width=100% class=tablaInferior>" +
								"      <tr>" +
								"        <td width=50% class=subtitulo1>Movimientos de Anticipos </td>" +
								"        <td width=50% class=barratitulo><img src=" + BASEURL + "/images/titulo.gif width=32 height=20> </td>" +
								"      </tr>" +
								"    </table><TABLE width=100% border=1 bordercolor=#999999 bgcolor=#F7F5F4  class=tablaInferior cellspading=0>" +
								"      <TR class=tblTitulo>" +
								"        <TH>CODIGO</TH>" +
								"        <TH>SECUENCIA</TH>" +
								"        <TH>DESCRIPCION</TH>" +
								"        <TH>FECHA</TH>" +
								"        <TH>CODIGO 1</TH>" +
								"        <TH>CODIGO 2</TH>" +
								"        <TH>DIFERENCIA</TH>" +
								"      </TR>";								 
               String bodyTi= "<TR><TD colspan=7 align=center class=bordereporte><br><br><br><span class=informacion>No tiene Tiempos de viajes asignados</span><br><br></TD></TR>";                  
			   if(listaTiempo!=null || listaTiempo.size()>0){
                   Iterator ittT=listaTiempo.iterator();
                   bodyTi=""; 
				   int i = 0;
                   while(ittT.hasNext()){
                      TiempoPlanilla tiempo = (TiempoPlanilla) ittT.next();
					  String clase = (i % 2 == 0 )? "filagris" : "filaazul";
                      bodyTi+="  <TR class=" + clase + "><TD align=center class=bordereporte>"+ tiempo.getCodigo() +"</TD><TD align=center class=bordereporte>"+ tiempo.getSecuencia() +"</TD><TD class=bordereporte>"+ tiempo.getDescripcion() +"</TD><TD class=bordereporte>"+ tiempo.getFecha() +"</TD><TD class=bordereporte>"+ tiempo.getCodigo1() +"</TD><TD class=bordereporte>"+ tiempo.getCodigo2() +"</TD><TD class=bordereporte>"+ tiempo.getDiferencia() +"</TD></TR>"; ;
					  i++;
                   }
               }
               String footerTi= "    </TABLE></td>" +
								"  </tr>" +
								"</table>" +
								"<br>" +
								"<center>" +
								"<BR>" +
								"<table width=800 border=0 cellspacing=0 cellpadding=0 align=center>" +
								"  <tr align=left>" +
								"    <td><img src=" + BASEURL  + "/images/botones/regresar.gif name=c_regresar id=c_regresar onMouseOver=botonOver(this); onMouseOut=botonOut(this); onClick=window.close()>" + 
								"  </tr>" +
								"</table>" +
								"</BODY></HTML>";
               String vistaTi = headTi + bodyTi + footerTi;
            %>
              <TABLE   class='comentario' width='90%' align='left'  cellpadding='0' cellspacing='0'>
                <TR>
                  <TD COLSPAN='2' class='comentario2'> <span class="letraresaltada">TIEMPOS Y MOVIMIENTOS</b></span><BR>
                        <BR>
                  </b></TD>
                </TR>
                <TR valign='top'>
                  <TD>&nbsp&nbsp&nbsp <a href="javascript:onclick=CambiarCampo('<%= vistaMov %>')" class="Simulacion_Hiper">Movimientos de Anticipos</a> <BR>
                      <BR></TD>
                </TR>
                <TR valign='top'>
                  <TD>&nbsp&nbsp&nbsp <a href="javascript:onclick=CambiarCampo('<%= vistaTi  %>')" class="Simulacion_Hiper">Tiempos de Viajes </a> <BR>
                      <BR></TD>
                </TR>
              </TABLE>
          <!--  *************************************  -->          </TD>
        </TR>
        <TR VALIGN='top'>
          <TD width='50%' class="fila"><br>
              <br>
              <TABLE   class='fondotabla' width='90%' align='right' cellpadding='0' cellspacing='0'>
                <TR valign='top'>
                  <TD COLSPAN='2' class='comentario2'> <span class="letraresaltada">FECHAS</b></span><BR>
                        <BR>
                  </b></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"><b>&nbsp&nbsp&nbsp DESPACHO </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getFechaDespacho()   %> <BR>
                  <BR></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"><b>&nbsp&nbsp&nbsp CREACION </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getFechaCreacion()   %> <BR>
                  <BR></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"><b>&nbsp&nbsp&nbsp CUMPLIDO </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getFechaCumplido()   %> <BR>
                  <BR></TD>
                </TR>
            </TABLE></TD>
          <TD width='50%' class="fila"><br>
              <br>
              <TABLE     class='fondotabla' width='90%' align='left'  cellpadding='0' cellspacing='0'>
                <TR>
                  <TD COLSPAN='2' class='comentario2'> <span class="letraresaltada">VALORES, PEAJES Y ACPM<BR>
                        </b></span><BR>
                  </b></TD>
                </TR>
                <TR VALIGN='top'>
                  <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp VLR PLANILLA </b></TD>
                  <TD width='50%' class="letraTitulo"> $<%=  com.tsp.util.Util.customFormat(planilla.getValor())  %> <BR>
                  <BR></TD>
                </TR>
                <TR VALIGN='top'>
                  <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp PROVEEDOR PEAJES </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getProveedorPeajes() %> <BR>
                  <BR></TD>
                </TR>
                <TR VALIGN='top'>
                  <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp CANTIDAD PEAJES </b></TD>
                  <TD width='50%' class="letraTitulo" > <%= String.valueOf(planilla.getCantidadPeajes()) %> <BR>
                  <BR></TD>
                </TR>
                <TR VALIGN='top'>
                  <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp VLR TOTAL PEAJES </b></TD>
                  <TD width='50%' class="letraTitulo" > $<%=  com.tsp.util.Util.customFormat(planilla.getValorPeajes() )   %> <BR>
                  <BR></TD>
                </TR>
                <TR VALIGN='top'>
                  <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp PROVEEDOR ACPM </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= planilla.getProveedorACPM() %><BR>
                  <BR></TD>
                </TR>
                <TR VALIGN='top'>
                  <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp GALONES </b></TD>
                  <TD width='50%' class="letraTitulo"> <%= String.valueOf(planilla.getGalonesACPM())    %> <BR>
                  <BR></TD>
                </TR>
                <TR valign='top'>
                  <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp VALOR TOTAL ACPM </b></TD>
                  <TD width='50%' class="letraTitulo"> $<%=  com.tsp.util.Util.customFormat(planilla.getValorACPM())      %> <BR>
                  <BR></TD>
                </TR>
            </TABLE></TD>
        </TR>
        <TR VALIGN='top'>
          <TD  width='50%' class="fila">
            <TABLE    class='comentario' width='90%' align='right'  cellpadding='0' cellspacing='0'>
              <TR valign='top'>
                <TD COLSPAN='2' class='comentario2'><span class="letraresaltada">AGENCIAS</b></span><BR>
                      <BR>
                </b></TD>
              </TR>
              <TR valign='top'>
                <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp AGE. ELABORACION: </b></TD>
                <TD width='50%' class="letraTitulo"> <%= planilla.getAgenciaDespacho() %> <BR>
                <BR></TD>
              </TR>
              <TR valign='top'>
                <TD width='50%' class="letraresaltada"> <b>&nbsp&nbsp&nbsp DESPACHADOR: </b></TD>
                <TD width='50%' class="letraTitulo"> <%= planilla.getDespachador()     %><br>
                    <%= planilla.getCedulaDespachador() %> <BR>
                <BR></TD>
              </TR>
              <TR valign='top'>
                <TD width='50%' class="letraresaltada"><b>&nbsp&nbsp&nbsp AGE. CUMPLIDO: </b></TD>
                <TD width='50%' class="letraTitulo"> <%= planilla.getAgenciaCumplido() %> <BR>
                <BR></TD>
              </TR>
            </TABLE>
            <BR>
            <BR>
            <BR>
            <BR>
            <BR>
            <BR>
          <BR>          </TD>
          <TD width='50%' class="fila"></TD>
        </TR>
      </TABLE></td>
    </tr>
  </table>
  <br>
  
<%
 }
 else{
  %><BR>
<table width="600"  border="2" align="center">
  <tr>
    <td><table width="100%" class="tablaInferior">
      <tr>
        <td width="50%" class="subtitulo1">Informaci&oacute;n de B&uacute;squeda </td>
        <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> </td>
      </tr>
    </table>
      <TABLE align='center' width='100%' class='tablaInferior'>
      <TR>
        <TD class='fila' align='center'><br>
            <br>
            <%= request.getParameter("comentario") %><br>
            <br>
            <br>
            <br>
            <br></TD>
      </TR>
    </TABLE></td>
  </tr>
</table>
    <%}%>
</body>
</html>