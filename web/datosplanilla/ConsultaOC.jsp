<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>

<html>
<head>
  <title>Listado de Remisiones</title>
  <link href="<%= BASEURL %>/css/Style.css" rel='stylesheet'>
  <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
  <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/date-picker.js"></script>
  <SCRIPT language=JavaScript1.2 
    src="<%=BASEURL%>/js/coolmenus3.js">
  </SCRIPT>
</head>
<body >
    <BR><BR><BR>   
    <center>    
    <a href='<%= BASEURL  %>/index.html'>Index</a>    
    
    
  <%
  if(request.getParameter("Opcion").equals("PRINCIPAL") ){ %>
       
       <FORM METHOD='POST' ACTION="<%=CONTROLLER%>?estado=Menu&accion=ConsultaOC&Opcion=ENTRADA">
       <TABLE class='fondotabla' BORDER='1' width='500'>
          <TR><TH class='titulo1'><br>CONSULTA DE OCS <br></TH></TR>
          <TR><TD class='titulo3'><BR>&nbsp&nbsp OSCOJA LA OPCION :<BR></TD></TR>
          <TR>
              <TD class='comentario' id='O1'  > 
                 <font id="op1" style=' width=100%'>
                   <input type='radio' name='radio' value='1' checked="checked" onclick='cambiar(op1,op2,op3)'>
                   <b>1.</b> Por numero de OC 
                 </font>         
              </TD> 
         </TR>
         <TR>
              <TD class='comentario' > 
                <font id="op2" style=' width=100%'>
                  <input type='radio' name='radio' value='2' onclick='cambiar(op2,op1,op3)'> 
                  <b>2.</b>OCs realizadas por rango de fechas 
                </font>         
              </TD>
         </TR>
         <TR>
              <TD class='comentario'>
                <font id="op3" style=' width=100%'>
                   <input type='radio' name='radio' value='3' onclick='cambiar(op3,op1,op2)'> 
                    <b>3.</b> OCs no cumplidas en un rango de fechas 
                </font>         
              </TD>
        </TR> 
        <TR>
           <TD align='center'><br> <input type='submit' value='Buscar'> <br><br></TD>
         </TR>
      </TABLE>
    </FORM>
  <%
  }
  else{
  %>
     <FORM METHOD='POST' ACTION="<%=CONTROLLER%>?estado=ACCION=ConsultaOC&accion=Buscar">
      <TABLE class='fondotabla' BORDER='1' width='400'>
       <%
          int tipoOpcion=Integer.parseInt(request.getParameter("radio"));
          switch (tipoOpcion){
            case 1:
                    %>
                    <TR><TH class='titulo1' colspan='2'> CONSULTA OC POR NUMERO<br><br></TH></TR>
                    <TR>
                        <TD class='comentario'>&nbsp Distrito:        </TD>
                        <TD>
                           <select name='distrito' class='comentario'>
                             <option value='FINV'>FINV</option>
                           </select>
                        </TD>
                    </TR>
                    <TR>
                       <TD class='comentario'>  &nbsp Numero de OC : </TD>
                       <TD>
                          <input type='text' name='numeroOC' class='comentario'> 
                       </TD>
                    </TR>
                    <TR>
                       <TD colspan='2' align='center'><br>
                          <input type='submit'  value='Buscar'> <br><br>
                          <input type='hidden' name='tipo' value='1'>
                       </TD>
                    </TR>
                    <%                
                    break;
            case  2:
                   %>
                    <TR><TH class='titulo1' colspan='2'>OCs REALIZADAS POR RANGO DE FECHAS<br><br></TH></TR>
                    <TR>
                        <TD class='comentario'>&nbsp Distrito:        </TD>
                        <TD>
                           <select name='distrito' class='comentario'>
                             <option value='FINV'>FINV</option>
                           </select>
                        </TD>
                    </TR>
                    <TR>
                        <TD class='comentario'>&nbsp Fecha Inicial </TD>
                        <TD>
                           <input type="text" size='10' readonly='true' name="FechaInicial" class='comentario'>
                           <input type="button" class='comentario' value="Escoger" onclick="javascript:show_calendar('FechaInicial')">
                        </TD>
                    </TR>
                    <TR>
                        <TD class='comentario'>&nbsp Fecha Final </TD>
                        <TD>
                          <input type="text" size='10' readonly='true' name="FechaFinal" class='comentario'>
                          <input type="button" class='comentario' value="Escoger" onclick="javascript:show_calendar('FechaFinal')">
                        </TD>
                    </TR>
                    <TR>
                       <TD colspan='2' align='center'><br>
                          <input type='button' onclick='validarFecha(this.form)' value='Buscar'> <br><br>
                          <input type='hidden' name='tipo' value='2'>
                       </TD>
                    </TR>
                   <%
                   break;
            case 3:
                  %>
                    <TR><TH class='titulo1' colspan='2'>OCs NO CUMPLIDAS POR RANGO DE FECHAS<br><br></TH></TR>
                    <TR>
                        <TD class='comentario'>&nbsp Distrito:        </TD>
                        <TD>
                           <select name='distrito' class='comentario'>
                             <option value='FINV'>FINV</option>
                           </select>
                        </TD>
                    </TR>
                    <TR>
                        <TD class='comentario'>&nbsp Fecha Inicial </TD>
                        <TD>
                           <input type="text" size='10' readonly='true' name="FechaInicial" class='comentario'>
                           <input type="button" class='comentario' value="Escoger" onclick="javascript:show_calendar('FechaInicial')">
                        </TD>
                    </TR>
                    <TR>
                        <TD class='comentario'>&nbsp Fecha Final </TD>
                        <TD>
                          <input type="text" size='10' readonly='true' name="FechaFinal" class='comentario'>
                          <input type="button" class='comentario' value="Escoger" onclick="javascript:show_calendar('FechaFinal')">
                        </TD>
                    </TR>
                    <TR>
                       <TD colspan='2' align='center'><br>
                          <input type='button' onclick='validarFecha(this.form)' value='Buscar'> <br><br>
                          <input type='hidden' name='tipo' value='3'>
                       </TD>
                    </TR>
                   <%                
                  break;
          }
       %>
     </TABLE>
    </FORM>
      <%
   }
  %>
</body>
</html>