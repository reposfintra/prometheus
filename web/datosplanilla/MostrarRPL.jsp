<!--
- Autor : Ing. Mario Fontalvo Solano
- Date  : 07 Marzo del 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, para la busqueda de los soportes a asignar.
--%>
<%@page contentType="text/html"%>
<%@page session   ="true"%> 
<%@page errorPage ="/error/ErrorPage.jsp"%>
<%@page import="java.util.*,com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.Plarem" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
	<title>Soportes Remesa</title>
	<link href="../css/estilostsp.css" rel="stylesheet">
	<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/validarRegistroCheque.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>	
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consulta Soportes Remesa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<center>

 <% String msg        = (String) request.getAttribute("msg");   
    Vector datos      = (Vector) request.getAttribute("datos");   
    if(datos!=null && !datos.isEmpty()) {		
	Plarem p0        = (Plarem) datos.get(0);
 %>
	
	<table border="2" width="500">
		<tr>	
			<td>
			
			<table width="100%"  border="0" cellpadding="0" cellspacing="1">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Datos de la planilla </td>
					<td class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table width="100%"  border="0" cellpadding="0" cellspacing="1">
				<tr >
					<td width="29%" class="fila" >&nbsp;Planilla </td>
					<td width="71%" class="letra" >&nbsp;<%= p0.getNumpla() %></td>
				</tr>
				<tr >
					<td width="29%" class="fila">&nbsp;Valor </td>
					<td width="71%" class="letra" >&nbsp;<%= Util.customFormat(p0.getVlrpla()) %></td>
				</tr>
				<tr >
					<td width="29%" class="fila">&nbsp;Centro de Costos (C)</td>
					<td width="71%" class="letra"  >&nbsp;<%= p0.getAccount_code_c() %></td>
				</tr>
			</table>			
			
						
			<table width="100%"  border="0" cellpadding="0" cellspacing="1">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Remesas Relacionadas </td>
					<td class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			
			
			
			<table width="100%" class="tablaInferior" >
				<tr class="tblTitulo" align="center">					
					<td width="85" align="center"  class="bordereporte">Remesa</td>				
					<td width="187" align="center"  class="bordereporte">Centro de Costos(I)</td>
					<td width="116" align="center"  class="bordereporte">Valor</td>
					<td width="80" align="center"  class="bordereporte">Porcentaje</td>
				</tr>
				
				<%
				double total = 0;
				for (int i = 0; i < datos.size(); i++) {
					Plarem p        = (Plarem) datos.get(i);
					String estilo   = (i % 2 == 0 )?"filagris":"filaazul";
					total += Double.parseDouble(p.getPorcent());
				%>
				<tr class="<%= estilo %>" align="center">					
					<td class="bordereporte" align="center" nowrap ><%= p.getNumrem() %></td>
					<td class="bordereporte" align="left" nowrap >&nbsp;<%= p.getAccount_code_i() %></td>				
 				    <td class="bordereporte" align="right" nowrap><%= Util.customFormat(p.getVlrrem()) %>&nbsp;</td>
					<td class="bordereporte" align="right" nowrap><%= p.getPorcent() %>%&nbsp;</td>
				</tr>				
				<% } %>		
				
				<tr class="tblTitulo" align="center">					
					<td class="bordereporte" align="right" nowrap colspan="3" >Total</td>
					<td class="bordereporte" align="right" nowrap><%= total %>%&nbsp;</td>
				</tr>					
			  </table>			
			</td>
		</tr>
	</table>	
	

 <% } %>
 <br>
 <img src="<%=BASEURL%>/images/botones/salir.gif"      name="imgsalir"     onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 
 
   <% if(msg!=null && !msg.trim().equals("")) { %>
        <p>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="229" align="center" class="mensajes"><%=msg%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table>                     
                    </td>
                </tr>
            </table>
        </p>
  <%}%>	 
</center>
</div>
</body>
</html>