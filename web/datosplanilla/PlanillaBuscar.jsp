<!--
- Autor : Ing. Jose de la rosa
- Date  : 22 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la impresion de las planillas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <title>Buscar Planillas</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    </head>
    <%-- Inicio Body --%>
	<body onLoad="redimensionar();" onResize="redimensionar();">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
			<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultar Planilla"/>
		</div>
		<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
        <%-- Inicio Formulario --%>
        <form name="forma" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Consulta">
        <%-- Inicio Tabla Principal --%>
        <table width="642" border="2" align="center">
            <tr>
                <td>
                    <%-- Inicio Tabla Secundaria --%>
                    <table width="100%" align="center"  class="tablaInferior">
                        <tr class="fila">
                            <td colspan="2" align="left" class="subtitulo1">&nbsp;Consulta De Planillas </td>
                            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr class="fila">
                            <td>Usuario</td>
                            <td><input name="c_usuario" class="textbox" type="text" id="c_usuario" size="12" maxlength="10"></td>
                            <td>Placa</td>
                            <td><input name="c_placa" type="text" class="textbox" id="c_placa" size="12" maxlength="12"></td>
                        </tr>
                        <tr class="fila" id="cantidad">
                            <td width="124"> Fecha Inicio </td>
                          <td width="182">
                                <input name="c_fecha_inicio" type="text" readonly class="textbox" id="c_fecha_inicio" size="12">			  
							  <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha_inicio);return false;" HIDEFOCUS>
							  <img src="<%=BASEURL%>/js/Calendario/cal.gif" alt="" name="popcal" width="15" height="15" border="0" align="absmiddle"></a></td>
                            <td width="125">Fecha Fin </td>
                          <td width="183">
                                <input name="c_fecha_fin" type="text" class="textbox" readonly id="c_fecha_fin" size="12">
								  <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.c_fecha_fin);return false;" HIDEFOCUS>
								  <img src="<%=BASEURL%>/js/Calendario/cal.gif" alt="" name="popcal" width="15" height="15" border="0" align="absmiddle"></a>
                            </td>
                        </tr>
                        <tr class="fila">
                            <td width="124"> Agencia Origen </td>
                            <td width="182"><input name="c_agencia_origen" type="text" class="textbox" id="c_agencia_origen" size="12" maxlength="3"></td>
                            <td width="125">Agencia Destino </td>
                            <td width="183"><input name="c_agencia_destino" type="text" class="textbox" id="c_agencia_destino" value="" size="12" maxlength="3">
                            </td>
                        </tr>
                        <tr class="fila">
                            <td width="124" align="left" > Agencia Despacho </td>
                            <td width="182" valign="middle">
                                <select name="c_agencia_despacho" class="textbox" id="c_agencia_despacho">
                                    <option value=""></option>
                                    <%model.agenciaService.cargarAgencias();
                                    Vector vec = model.agenciaService.getAgencias();
                                    for(int i = 0; i<vec.size(); i++){	
                                        Agencia a = (Agencia) vec.elementAt(i);	%>
                                        <option value="<%=a.getId_agencia()%>"><%=a.getNombre()%></option>
                                    <%}%>
                                </select></td>
                            <td width="125">Cedula Conductor</td>
                            <td width="183"><input name="c_cedula" type="text" class="textbox" id="c_cedula" value="" size="12">          </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <p>
            <div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar Planillas" name="buscar"  onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
            <img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
        </p>
        </form>
		</div>
		<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>
