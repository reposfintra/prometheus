<!--
- Autor : Ing. Jose de la rosa
- Date  : 10 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja las listas de las sanciones.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
    <head>
        <title>Listado De Consulta Historial Viajes</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
    </head>
    <%-- Inicio Body --%>
    <body>
<%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Planilla pla = model.planillaService.getPla();
    Vector vec = model.planillaService.ReporteVector();
    Planilla p;
    if ( vec.size() >0 ){  
%>
        <%-- Inicio Tabla Principal --%>
        <table width="1400" border="2" align="center">
            <tr>
                <td>
                    <%-- Inicio Tabla Cabecera --%>
                    <table width="100%" align="center">
                        <tr>
                            <td width="373" class="subtitulo1">&nbsp;Datos Consulta Viajes </td>
                            <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                    </table>
                    <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                        <tr class="tblTitulo" align="center">
                            <td width="6%" align="center">Estado Despacho</td>
                            <td width="4%"  align="center">Planilla</td>
                            <td width="5%" align="center">Estado Remesa </td>
                            <td width="5%" align="center">Remesa</td>
                            <td width="6%" align="center">Fecha</td>
                            <td width="13%" align="center">Nombre Conductor </td>
                            <td width="9%" align="center">Agencia Despacho </td>
                            <td width="9%" align="center">Origen Planilla</td>
                            <td width="9%" align="center">Destino Planilla </td>
                            <td width="16%" align="center">Cliente</td>
                            <td width="9%" align="center">Origen Remesa </td>
                            <td width="9%" align="center">Destino Remesa </td>
                        </tr>
                        <pg:pager
                        items="<%=vec.size()%>"
                        index="<%= index %>"
                        maxPageItems="<%= maxPageItems %>"
                        maxIndexPages="<%= maxIndexPages %>"
                        isOffset="<%= true %>"
                        export="offset,currentPageNumber=pageNumber"
                        scope="request">
                        <%-- keep track of preference --%>
                        <%
                        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
                            p = (Planilla) vec.elementAt(i);%>
                            <pg:item>
                                <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
                                    onClick="window.open('<%=CONTROLLER%>?estado=ConsultaOCNormal&accion=Buscar&tipo=1&distrito=TSP&numeroOC=<%=p.getNumpla()%>&numeroOT=<%=p.getNumrem()%>','','scrollbars=no, resizable=yes')">
                                    <td width="6%" class="bordereporte">&nbsp;<%=p.getReg_status()%></td>
                                    <td width="4%" class="bordereporte"><%=p.getNumpla()%></td>  
                                    <td width="5%" class="bordereporte">&nbsp;<%=p.getStatus_220()%></td>
                                    <td width="5%" class="bordereporte"><%=p.getNumrem()%></td>
                                    <td width="6%" class="bordereporte"><%=p.getFecdsp()%></td>
                                    <td width="13%" class="bordereporte"><%=p.getNomCond()%></td>
                                    <td width="9%" class="bordereporte"><%=p.getAgcpla()%></td>
                                    <td width="9%" class="bordereporte"><%=p.getOripla()%></td>
                                    <td width="9%" class="bordereporte"><%=p.getDespla()%></td>
                                    <td width="16%" class="bordereporte"><%=p.getNitpro()%></td>
                                    <td width="9%" class="bordereporte"><%=p.getNomori()%></td>
                                    <td width="9%" class="bordereporte"><%=p.getNomdest()%></td>
                                </tr>
                            </pg:item>
                        <%}%>
                        <tr  class="pie" align="center">
                            <td td height="20" colspan="12" nowrap align="center">
                                <pg:index>
                                    <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
                                </pg:index> 
                            </td>
                        </tr>
                        </pg:pager>
                    </table>
                </td>
            </tr>
        </table>
        <br>
        <%} else { %>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
                                <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="78">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
        <%}%>
        <table width="1400" border="0" align="center">
            <tr>
                <td>
                    <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/datosplanilla&pagina=PlanillaBuscar.jsp&titulo=Consulta Viajes'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                </td>
            </tr>
        </table>
    </body>
</html>
