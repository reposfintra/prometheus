<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Extrafletes...</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<body onLoad="redimensionar();" onresize="redimensionar()" >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ExtraFletes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <table width="81%"  border="2" align="center">
	<tr>
	  <td>
        <table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" colspan=2 class="subtitulo1"><span class="titulo"><strong>
              
EXTRAFLETES<br>
            </strong></span></td>
            <td width="302" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%"   border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
	<tr bordercolor="#999999" class="tblTitulo">
	  <td height="0" colspan="3">COSTOS</td>
      <td>INGRESOS</td>
	  <td>LIQUIDACION</td>
	</tr>
	<tr bordercolor="#999999" class="tblTitulo" align="center">
	  <td height="24" >DESCRIPCION</td>
	  <td width="19%">VALOR COSTO</td>
	   <td width="14%">CANTIDAD</td>
      <td width="14%">VALOR</td>
	  <td width="13%">TOTAL</td>
	</tr>
	<%
	Vector fletes = model.sjextrafleteService.getFletes();
		for(int i= 0; i<fletes.size(); i++){
			SJExtraflete s =(SJExtraflete) fletes.elementAt(i);
		%>
	<tr class="<%=i%2==0?"filagris":"filaazul"%>">
	  <td class="bordereporte">	    <%=s.getDescripcion()%></td>
      <td class="bordereporte"><%=com.tsp.util.Util.customFormat(s.getValor_costo())%> <%=s.getMoneda_costo()%></td>
      <td class="bordereporte"><%=s.getCantidad()%></td>
      <td class="bordereporte"><%=com.tsp.util.Util.customFormat(s.getValor_ingreso())%></td>
	  <td class="bordereporte"><%= com.tsp.util.Util.customFormat((s.getCantidad() * s.getValor_ingreso()))%></td>
	</tr>
	<%}%>
	
  </table>  </td>
    </tr>
  </table>
  <br>
  <div align="center">
  <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
 </div>
 </div>
</body>
</html>
