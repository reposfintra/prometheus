<!--
     - Author(s)       :      JULIO BARROS RUEDA
     - Date            :      19/12/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: formulario q permite ingresar a los Extractos para aprobar los prontopagos
 --%>


<%@page contentType="text/html; charset=iso-8859-1"%>
<%@page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% TreeMap Agencia = model.agenciaService.getAgencia();
 Agencia.put(" TODAS","TODAS");
%>

<html>
<head>
	<title>.:: Reporte de Cumplidos</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
	<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
	
	<script>
	   function enviarFormulario(CONTROLLER,frm){
		   if((frm.fechai.value != "" )&&(frm.fechaf.value != "" )){
				 if(frm.fechai.value <= frm.fechaf.value){
				 	
					var tem1='';
					var tem2='';
					for (i=0;i<(frm.fechai.value).length;i++){
      					if (  (frm.fechai.value).charAt(i)  != '-')
							tem1+= (frm.fechai.value).charAt(i);
						else
							tem1+= '/';
   					}
					for (i=0;i<(frm.fechaf.value).length;i++){
      					if (  (frm.fechaf.value).charAt(i)  != '-')
							tem2+= (frm.fechaf.value).charAt(i);
						else
							tem2+= '/';
   					}
    				var fecha1 = new Date(tem1);
    				var fecha2 = new Date(tem2);
    				var factor = 1000*60*60*24;
    				var time   = (fecha2.getTime() - fecha1.getTime()) / factor;
    				//alert (time);
				    if(time<=31){
				 		document.mod.src='<%=BASEURL%>/images/botones/aceptarDisable.gif';
           				document.mod.onmouseover = new Function('');
           				document.mod.onmouseout  = new Function('');
           				document.mod.onclick     = new Function('');
					
						frm.submit();
					}else{
						alert("La fecha no debe superar los 31 dias");     
					}
				 }else{
					 alert("La fecha inicial debe ser menor o igual a la final");     
				 }
		   }else{
			   alert("La campos fecha no pueden estar vacios"); 
		   }                                                                                      
	   }
	</script>
	
</head>
	
	<body onLoad="window.opener.location.reload();">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
			<jsp:include page="/WEB-INF/toptsp.jsp?encabezado= Extractos "/>
		</div>
		<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
			<form action="<%=CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=REPORTECUMPLIDOS" method="post" name="formulario">
				<table border="2" align="center" width="426">
				  <tr>
					<td >
					  <table width="99%" align="center">
						<tr>
						  <td width="392" height="24"  class="subtitulo1"><p align="left">Filtro  Reporte de Cumplidos </p></td>
						  <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
					  </table>
					  <table width="99%" align="center">
						<tr class="fila">
							<td><strong>&nbsp; Fecha inicial </strong></td>
							<td>
							  <input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='<%=com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.MesActual()+"-01"%>' readonly>
							  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechai);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
								border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
						 </tr>
						 <tr class="fila" >
							<td><strong>&nbsp; Fecha final </strong></td>
							<td>
							  <input name='fechaf' type='text' class="textbox" id="fechaf" style='width:120' value='<%=com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.MesActual()+"-"+com.tsp.util.Util.diasDelMes(com.tsp.util.Util.MesActual())%>' readonly>
							  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaf);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
							  border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
						  </tr>
					   </table>
					 </td>
				  </tr>
				</table>
				<br>
				<table width="595" border="0" align="center">
				  <tr>
					<td align="center">
					  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="javascript:enviarFormulario('<%=CONTROLLER%>',formulario);"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
					  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
					</tr>
				</table>
				<br>
		 <%if (request.getParameter("msj") != null ){%>      
            <table border="2" align="center">
              <tr>
                <td>
					<table width="100%"  border="0" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  		<tr>
                    		<td width="500" align="center" class="mensajes"><%= request.getParameter("msj") %></td>
                    		<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<script>
								document.mod.src='<%=BASEURL%>/images/botones/aceptarDisable.gif';
								document.mod.onmouseover = new Function('');
			           			document.mod.onmouseout  = new Function('');
           						document.mod.onclick     = new Function('');
							</script>
                    		<td width="58">&nbsp; </td>
                  		</tr>
                	</table>
				</td>
              </tr>
     </table>               
     <%}%>   
				
			</form>
		</div>
	<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
		 </iframe>
		 
		 
	
	</body>
	</html>