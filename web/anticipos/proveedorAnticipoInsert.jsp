<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
    <title>Modificar proveedor ACPM</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
	<%
		String style = "simple";
	    String position =  "bottom";
    	String index =  "center";
		int maxPageItems = 10;
		int maxIndexPages = 10;   
	%>
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Asignar Proveedor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<table width="661" border="2" align="center">
	<tr>
        <td colspan="5">
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='5'>                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='3'>Escoja El Anticipo</td>
                                <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan='5'>
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">                  
								<td colspan="2" nowrap align="center">ANTICIPO</td>
								<td width="25%" nowrap align="center">TIPO DESCUENTO</td>
								<td width="25%" nowrap align="center">DISTRITO</td>
								<td width="25%" nowrap align="center">STANDARD JOB</td>
  							</tr>
							<%
								Vector list1 = model.anticiposService.getAnticipos();
							%>
								<pg:pager
                                    items="<%=list1.size()%>"
                                    index="<%= index %>"
                                    maxPageItems="<%= maxPageItems %>"
                                    maxIndexPages="<%= maxIndexPages %>"
                                    isOffset="<%= true %>"
                                    export="offset,currentPageNumber=pageNumber"
                                    scope="request">
                                  <%-- keep track of preference --%>
								  <%
									 if(model.anticiposService.getAnticipos().size()>0){											
											for (int i = offset.intValue(),l = Math.min(i + maxPageItems, list1.size());i < l; i++){%>
										<pg:item>
								  <%
										Anticipos ant = (Anticipos) list1.elementAt(i);
										String codigo = ant.getAnticipo_code();
										String nombre = ant.getAnticipo_desc();
										String tipo_des = ant.getTipo_s();
										String sj = ant.getSj_nombre();
										String sj_no = ant.getSj(); 							
										String distrito = ant.getDstrct();
										if(tipo_des.equals("V")){
											tipo_des="Valor";
										}else{
											tipo_des="Porcentaje";
										}
													
								%>
						  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" title="Modificar Anticipo..." onClick="window.location='<%=CONTROLLER%>?estado=Anticipos&accion=Search&codant=<%=codigo%>&distrito=<%=distrito%>&sj=<%=sj_no%>&proveedores=ok'" >
							<td width="61" nowrap class="bordereporte" align="center"><%=codigo%></td>
							<td width="110" nowrap class="bordereporte" align="center"><%=nombre%></td>
							<td nowrap class="bordereporte" align="center"><%=tipo_des%></td>
							<td nowrap class="bordereporte" align="center"><%=distrito%></td>
							<td nowrap class="bordereporte" align="center"><%=sj%></td>
						  </tr>
									  </pg:item>
									  <%}
										  }%>
						  <tr class="filagris">
                            	<td height="30" colspan="5" nowrap><pg:index><jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/></pg:index></td>
						  </tr>
							</pg:pager>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<br><%if(request.getAttribute("pro")!=null){%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=ProveedorAnticipo&accion=Insert&cmd=show">
  <table width="677" border="2" align="center">
	  <tr>
		<td colspan="3">
			<table width="100%" align="center" class="tablaInferior"> 
				<tr>
					<td colspan='3'>                
						<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
							<tr>
								<td width="50%" class="subtitulo1">Escoja El Proveedor Para Asociar El Anticipo</td>
								<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='3'>
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
							<tr class="tblTitulo">                      
							  <td colspan="3" nowrap align="center">NIT:</td>
							  <td width="152" nowrap align="center">CIUDAD</td>
							  <td width="105" nowrap align="center">DISTRITO</td>
    						</tr>
									<%
									 if(model.proveedoresService.getProveedores().size()>0){
											Vector list = model.proveedoresService.getProveedores();
											for(int i =0; i<list.size();i++){%>
											<%Proveedores pa = (Proveedores) list.elementAt(i);					
													String  nit= pa.getNit();
													String desc=pa.getNombre();
													String ciudad=pa.getCity_code();
													String distrito= pa.getDstrct();
													String sucursal = pa.getSucursal();
													String codigo = request.getParameter("codant");
									%>
									<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand">
									  <td width="16" nowrap class="bordereporte">									  
										<%if(model.proveedoresService.estaAnticipoProvee(nit, sucursal,codigo)){%>
										<input type="checkbox" name="<%=nit%>/<%=sucursal%>" value="checkbox" checked>
										<%}else{%>
										<input type="checkbox" name="<%=nit%>/<%=sucursal%>" value="checkbox">
									  <%}%></td>
									  <td width="74" nowrap class="bordereporte" align="center"><%=nit%></td>
									  <td width="276" nowrap class="bordereporte" align="center"><%=desc%> <%=sucursal%> </td>
									  <td nowrap class="bordereporte" align="center"><%=ciudad%></td>
									  <td nowrap class="bordereporte" align="center"><%=distrito%>
									  <input name="ant" type="hidden" id="ant" value="<%=codigo%>"></td>
									</tr>
									<%}
									  }%>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
  <table width="677" align="center">
  	<tr>
		<td align="center">
		<img title='Ingresar anticipo' src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="forma.submit();"></img>
		<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img></td>
	</tr>
  </table>    
  </div>
</form>
<%}%>
</div>
</body>
</html>
