<%-- 
    Document   : AnularAnticipos
    Created on : 11/05/2010, 10:26:19 AM
    Author     : maltamiranda
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<html>
    <head>
        <title>Listado Anticipos Pagos Terceros</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
        <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
        <link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">

        <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
        <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
        <title>Anular Anticipos</title>
    </head>
    <body onLoad="redimensionar();" onResize="redimensionar();" onclick="">
        <form action="" method='post' name='formulario' id='formulario' >
            <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
                <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular Anticipos Pagos Terceros"/>
            </div>

            <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
                <%  Usuario usuario = (Usuario) session.getAttribute("Usuario");
                            String user = usuario.getLogin();
                            String colums = "13";
                            ArrayList data = model.AnticiposPagosTercerosSvc.ObtenerAnticiposNoTransferidos(user);
                            String title = "ANULAR ANTICIPOS";
                            String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
                            String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
                %>
                <table width="98%" border="2" align="center" onclick="try{if(event.target.type!='checkbox' && event.target.type!='text'){wclose();}}catch(e){if(event.srcElement.type!='checkbox' && event.srcElement.type!='text'){wclose();}}">
                    <tr>
                        <td>
                            <table width='100%' align='center' class='tablaInferior'>

                                <tr>
                                    <td>
                                        <table width='100%' align='center' class='tablaInferior'>

                                            <tr class="barratitulo">
                                                <td colspan='<%= colums%>' >
                                                    <table cellpadding='0' cellspacing='0' width='100%'>
                                                        <tr>
                                                            <td align="left" width='70%' class="subtitulo1">&nbsp;<%=title%></td>
                                                            <td align="left" width='*'  >
                                                                <img alt="" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="fila">
                                                <td width='100%' colspan='2'>

                                                    <table width='100%' border="1"  bgcolor="#F7F5F4" align="center" id="mytable">
                                                        <tr class="tblTitulo" >
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" height='25' colspan='16' >DATOS ANTICIPO        </TH>
                                                        </tr>
                                                        <tr class="tblTitulo" >
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" > <input type='checkbox'  id='All' onclick="Select_all(this.form,this,'00');">  </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >No                </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >OBSERVACION       </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >AGENCIA           </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >CONDUCTOR         </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >PROPIETARIO       </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >PLACA             </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >PLANILLA          </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >FECHA             </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >REANT             </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >VALOR             </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >ASESOR            </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >REFERENCIADO      </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >USER CREACION     </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >FECHA CREACION    </TH>
                                                            <TH   nowrap style="font-size:smaller; font-weight: bold" >DES CONCEPT       </TH>
                                                        </tr>
                                                        <%for (int i = 0; i < data.size(); i++) {%>
                                                        <tr class='<%= (i % 2 == 0 ? "filagris" : "filaazul")%>' id='<%=i+1%>'   style=" font-size:smaller" >
                                                            <%ArrayList arl = (ArrayList) data.get(i);%>
                                                            <td class="bordereporte" align='center' nowrap style="font-size:smaller">
                                                                <input type='checkbox' name='anticipo' id="<%=arl.get(0)%>" value="<%=arl.get(1)%>" alt="<%=i+1%>" onmouseover="fila=this.alt;if(this.checked)control(event,'obs<%=arl.get(1)%>')" onclick="control(event,'obs<%=arl.get(1)%>');Select_all(this.form,this,'<%=arl.get(14)%>');">
                                                            </td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=i + 1%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <input type="text" style="width: 500px" alt="<%=i+1%>" readonly onclick="fila=this.  alt;control(event,'obs<%=arl.get(1)%>')" name="obs<%=arl.get(1)%>" id="obs<%=arl.get(1)%>"> </td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(2)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(3)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(4)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(5)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(6)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(7)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(8)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(9)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(10)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(11)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(12)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%=arl.get(13)%></td>
                                                            <td class="bordereporte" nowrap style="font-size:smaller"> <%= (((String) arl.get(14)).equals("10")) ? "GASOLINA" : ((((String) arl.get(14)).equals("01")) ? "ANTICIPO" : ((((String) arl.get(14)).equals("50")) ? "PRONTOPAGO" : ""))%></td>
                                                        </tr>
                                                        <%}%>
                                                        <!--AQUI SE CARGAN LOS DATOS DE LAM TABLA-->

                                                    </table>
                                                </td>
                                            </tr>

                                            <tr class="fila">
                                                <td width='100%' colspan='2'>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <br>
                <br>
                <div align="center">
                    <img alt="" src="<%=BASEURL%>/images/botones/anular.gif"       height="21"  title='Anular'    onclick="send(formulario,'<%=CONTROLLER%>?estado=Anticipos&accion=PagosTerceros')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer">

                    <div id="contenido" align="center" style="width: 200px;height: 100px; visibility: hidden; background-color: white">
                        <textarea id="cont" style="width: 95%;height: 95%;"  onkeyup="$(argumento).value=this.value;"></textarea>
                    </div>
                </div>
            </div>
            <script>
                var win ;
                var argumento;
                var fila;
                function control(e,arg)
                {   argumento=arg;
                    $('cont').value=$(arg).value;
                    if(!win){
                        openWin(e,arg);
                    }
                    else{
                        $('cont').value=$(arg).value;
                        win.setLocation(e.clientY,e.clientX+20);
                        win.setTitle("OBSERVACION ITEM "+fila);
                    }
                    $('cont').focus();
                }

                function wclose(){
                    if(win){
                        $('contenido').style.visibility='hidden';
                        win.destroy();
                        win=null;
                    }
                }
                function wclose2(){
                    $('contenido').style.visibility='hidden';
                    win=null; 
                }
                function Select_all(theForm,ele,tipo){
                    if(!ele.checked){
                        wclose();
                    }
                    if (ele.id=='All' && tipo=='00'){
                        for (i=0;i<theForm.length;i++)
                            if (theForm.elements[i].type=='checkbox')
                                theForm.elements[i].checked=ele.checked;
                    }else{
                        for (i=0;i<theForm.length;i++)
                            if (theForm.elements[i].type=='checkbox' && theForm.elements[i].id==ele.id && tipo=='50')
                                theForm.elements[i].checked=ele.checked;
                    }

                }


                function openWin(e,arg){
                    $('contenido').style.visibility='visible';
                    win= new Window(
                    {   id: "detalles",
                        title: "OBSERVACION ITEM "+fila,
                        width:$('contenido').width,
                        height:$('contenido').heigth,
                        destroyOnClose: true,
                        onClose:wclose2,
                        maximizable:false,
                        resizable: false,
                        closable:false,
                        minimizable:false
                    });
                    win.setContent('contenido', true, true);
                    win.show(false);
                    win.setLocation(e.clientY, e.clientX);
                }
                function send (theForm,url)
                {   p = "evento=ANULAR";
                    openInfoDialog('<b> Ejecutando<br/>Por favor espere...</b>');
                    for (i=0;i<theForm.length;i++)
                    {   if (theForm.elements[i].type=='checkbox' && theForm.elements[i].id!='All' && theForm.elements[i].checked)
                        {   p=p+"&id=" + theForm.elements[i].value + "&obs"+theForm.elements[i].value+"="+ $("obs"+theForm.elements[i].value).value;
                            index=theForm.elements[i].parentNode.parentNode.rowIndex;
                            $('mytable').deleteRow(index);
                        }
                    }
                    new Ajax.Request(
                    url,
                    {   method: 'post',
                        parameters: p,
                        onComplete: complete
                    });
                }
                function complete(response)
                {   Dialog.closeInfo();
                    Dialog.alert(response.responseText, {
                        width:250,
                        height:100,
                        windowParameters: {className: "alphacube"}
                    });
                }
                function openInfoDialog(mensaje) {
                    Dialog.info(mensaje, {
                        width:250,
                        height:100,
                        showProgress: true,
                        windowParameters: {className: "alphacube"}
                    });
                }
            </script>
        </form>
    </body>
</html>
