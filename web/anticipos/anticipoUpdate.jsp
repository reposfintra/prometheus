<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
    <title>Modificar proveedor ACPM</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">    
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/validarDocumentos.js"></script> 
</head>
<%
	String style = "simple";
	String position =  "bottom";
	String index =  "center";
	int maxPageItems = 10;
	int maxIndexPages = 10;   
%>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Modificar Anticipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <form name="form2" method="post" action="<%=CONTROLLER%>?estado=Acpm&accion=Search&num=1">
 <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
    <table width="661" border="2" align="center">
		<tr>
			<td colspan="4">
				<table width="100%" align="center" class="tablaInferior"> 
					<tr>
						<td colspan='4'>                
							<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
								<tr>
									<td width="50%" class="subtitulo1" colspan='4'>Escoja El Anticipo</td>
									<td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan='4'>
							<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
								<tr class="tblTitulo">                	  	        
									<td colspan="2" nowrap align="center">ANTICIPO</td>
									<td width="142" nowrap align="center">TIPO DESCUENTO </td>
									<td width="155" nowrap align="center">DISTRITO</td>
									<td width="139" nowrap align="center">STANDARD JOB</td>
								</tr>
									<%
										Vector list = model.anticiposService.getAnticipos();																						
									%>
									<pg:pager
										items="<%=list.size()%>"
										index="<%= index %>"
										maxPageItems="<%= maxPageItems %>"
										maxIndexPages="<%= maxIndexPages %>"
										isOffset="<%= true %>"
										export="offset,currentPageNumber=pageNumber"
										scope="request">									  
									 <%
										 if(model.anticiposService.getAnticipos().size()>0){																								
												for (int i = offset.intValue(),l = Math.min(i + maxPageItems, list.size());i < l; i++){%>
												<pg:item>
													<%
													Anticipos ant = (Anticipos) list.elementAt(i);
													String codigo = ant.getAnticipo_code();
													String nombre = ant.getAnticipo_desc();
													String tipo_des = ant.getTipo_s();
													String sj = ant.getSj_nombre();
													String sj_no = ant.getSj(); 
													
													String distrito = ant.getDstrct();
													if(tipo_des.equals("V")){
														tipo_des="Valor";
													}else{
														tipo_des="Porcentaje";
													}													
									%>
								<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" title="Modificar Anticipo..." onClick="window.location='<%=CONTROLLER%>?estado=Anticipos&accion=Search&codant=<%=codigo%>&distrito=<%=distrito%>&sj=<%=sj_no%>'">						
								  <td width="61" nowrap class="bordereporte" align="center"><%=codigo%></td>
									<td width="110" nowrap class="bordereporte" align="center"><%=nombre%></td>
									<td nowrap class="bordereporte" align="center"><%=tipo_des%></td>
									<td nowrap class="bordereporte" align="center"><%=distrito%></td>
									<td nowrap class="bordereporte" align="center"><%=sj%></td>
								</tr>
									</pg:item>
										<%}
										  }%>
								<tr class="filagris">
                            		<td height="30" colspan="5" nowrap><pg:index><jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/></pg:index></td>
                          		</tr>
                          </pg:pager>
    					</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
    </form>

    <form name="forma" method="post" action="<%=CONTROLLER%>?estado=Anticipos&accion=Update&cmd=show">
  <%if(request.getAttribute("ant")!=null){
  		Anticipos ant = (Anticipos) request.getAttribute("ant");
		String codigo = ant.getAnticipo_code();
		String nombre = ant.getAnticipo_desc();
		String tipo_des = ant.getTipo_s();
		String sj = ant.getSj(); 
		String moneda = ant.getMoneda();
		String distrito = ant.getDstrct();
		float valor = ant.getValor();
		String migracion = ant.getCodmigra();
		String indicador = ant.getIndicador();
  %>
 
    <table width="543" border="2" align="center">
		<tr>
			<td colspan="2">
				<table width="100%" align="center">
			        <tr>
				        <td nowrap class="subtitulo1">Modificar Anticipo</td>
						<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
			        </tr>
					<tr>
					  <td width="166" rowspan="2" nowrap class="fila">Descuento</td>
						<td width="342" nowrap class="fila"><%=codigo%> <input name="codant" type="hidden" id="codant" value="<%=codigo%>"></td>
					</tr>
					<tr>
					  <td nowrap class="fila"><%=nombre%> <input name="descant" type="hidden" id="descant" value="<%=nombre%>"></td>
					</tr>
					<tr>
					  <td nowrap class="fila">Distrito</td>
						<td nowrap class="fila">
							<select name="distrito" class="textbox" id="distrito">
								<option value="FINV" selected>FINV</option>
							</select>
					  <input name="distrito" type="hidden" id="distrito" value="<%=distrito%>">					  </td>
					</tr>
					<tr>
						<td nowrap class="fila">Standard Job</td>
						<td nowrap class="fila"><input name="sj" type="text" class="textbox" id="sj" value="<%=sj%>"></td>						 
				  	</tr>
					<tr>
					  <td nowrap class="fila">Tipo de Descuento</td>
						  <td nowrap class="fila">
							<select name="tipo_des" class="textbox" id="tipo_des">
							  <option value="V">Valor</option>
							  	<%if (tipo_des.equals("V")){%>
							  <option value="P">Porcentaje</option>
							  <option value="V" selected>Valor</option>
						  		<%}
								else{%>
									  <option value="P" selected>Porcentaje</option>
								<%}%>
					  </select>					  </td>  
				  </tr>
					<tr>
					  <td nowrap class="fila">Valor</td>
					  <td nowrap class="fila"><input name="valor" type="text" class="textbox" id="valor"  onKeyPress="soloDigitos(event,'decOK')" value="<%=valor%>">
						<select name="moneda" class="textbox" id="moneda">
						  <%if(moneda.equals("PES")){%>
						  <option value="PES" selected>Pesos</option>
						  <option value="BOL">Bolivar</option>
						  <option value="DOL">Dolares</option>
						  <%}else if(moneda.equals("BOL")){%>
						  <option value="PES">Pesos</option>
						  <option value="BOL" selected>Bolivar</option>
						  <option value="DOL">Dolares</option>
						  <%}else if(moneda.equals("DOL")){%>
						  <option value="PES">Pesos</option>
						  <option value="BOL">Bolivar</option>
						  <option value="DOL" selected>Dolares</option>
						  <%}%>
				  	  </select>					  </td>		  
					</tr>
					<tr>
					  <td nowrap class="fila">Aplicar a </td>
					  <td nowrap class="fila">
						  <select name="indicador" class="textbox" id="select">
							<%if(indicador.equals("S")){%>
							<option value="S" selected>Saldo</option>
							<option value="V">Valor</option>
							<%}else if(indicador.equals("V")){%>
							<option value="S">Saldo</option>
							<option value="V" selected>Valor</option>
							<%}%>
					  </select>					  </td>          
					</tr>
					<tr>
					  <td nowrap class="fila">Codigo Migracion </td>
					  <td nowrap class="fila"><input name="migracion" type="text" class="textbox" id="migracion" value="<%=migracion%>" size="2" maxlength="1"></td>
				  </tr>
    		</table>
			</td>
			</tr>
			</table>
	<br>
	<table width="543" align="center">
		<tr>
			<td>
        	<td align="center">
				<img title='Modificar reporte' src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="TCamposLlenos();"></img>
            	<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.close();"></img>
			</td>
			</td>
			</tr>
    </table>
  <%}%>
    <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
    </table>
    </form>
	</div>
</body>
</html>
