<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar proveedor ACPM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script> 

</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Ingresar Anticipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Anticipos&accion=Insert&cmd=show" onSubmit="return ValidarFormAnticipos(this);">
  <table width="530" border="2" align="center">
    <tr>
      <td nowrap class="subtitulo1">Ingresar Anticipo</td>
	  <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    </tr>
    <tr class="fila">
      <td width="165" nowrap>Codigo del Concepto</td>
      <td width="331" nowrap bgcolor="<%=request.getAttribute("error")%>"><input name="codant" type="text" class="textbox" id="codant" value="<%=request.getParameter("codant")%>" maxlength="15">          </td>
    </tr>
    <tr class="fila">
      <td nowrap>Descripcion</td>
      <td width="331" nowrap><input name="descant" type="text" class="textbox" id="descant" value="<%=request.getParameter("descant")%>" maxlength="15"></td>
    </tr>
    <tr class="fila">
      <td nowrap>Distrito</td>
      <td nowrap>
	  	<select name="distrito" class="textbox" id="distrito">
        	<option value="FINV" selected>FINV</option>
      	</select>
	  </td>
    </tr>
    <tr class="fila">
      <td nowrap>Standard Job</td>
      <td nowrap bgcolor="<%=request.getAttribute("error1")%>"><input name="sj" type="text" class="textbox" id="sj" value="<%=request.getParameter("sj")%>">
	</tr>
    <tr class="fila">
      <td nowrap>Tipo de Descuento</td>
      <td nowrap><select name="tipo_des" class="textbox" id="tipo_des">
	  <option value="V">Valor</option>
	  <%if (request.getParameter("tipo_des").equals("V")){%>
        <option value="P">Porcentaje</option>
        <option value="V" selected>Valor</option>
		<%}
		else{%>
        <option value="P" selected>Porcentaje</option>
		<%}%>
      </select></td>
    </tr>
    <tr class="fila">
      <td nowrap>Valor</td>
      <td nowrap>
	  	<input name="valor" type="text" class="textbox" id="valor"  onKeyPress="soloDigitos(event,'decOK')" value="<%=request.getParameter("valor")%>">
        <select name="moneda" class="textbox" id="moneda">
		<%if(request.getParameter("moneda").equals("PES")){%>
          <option value="PES" selected>Pesos</option>
          <option value="BOL">Bolivar</option>
          <option value="DOL">Dolares</option>
		  <%}else if(request.getParameter("moneda").equals("BOL")){%>
          <option value="PES">Pesos</option>
          <option value="BOL" selected>Bolivar</option>
          <option value="DOL">Dolares</option>
		  <%}else if(request.getParameter("moneda").equals("DOL")){%>
          <option value="PES">Pesos</option>
          <option value="BOL">Bolivar</option>
          <option value="DOL" selected>Dolares</option>
		  <%}%>
        </select></td>
    </tr>
    <tr class="fila">
      <td nowrap>Aplicar a</td>
      <td nowrap>
	  <select name="indicador" class="textbox" id="indicador">
        <%if(request.getParameter("indicador").equals("S")){%>
		<option value="S" selected>Saldo</option>
        <option value="V">Valor</option>
		<%}else if(request.getParameter("indicador").equals("V")){%>
		<option value="S">Saldo</option>
        <option value="V" selected>Valor</option>
		<%}%>
      </select></td>
    </tr>
    <tr class="fila">
      <td nowrap>Codigo Migracion</td>
      <td nowrap><input name="migracion" type="text" class="textbox" id="migracion" value="<%=request.getParameter("migracion")%>" size="2" maxlength="1" ></td>
    </tr>
  </table>
  <br>
  <table align="center" width="530">
  	<tr>
		<td align="center">
			<img title='Ingresar anticipo' src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="TCamposLlenos();">			
			<img title='Reiniciar valores' src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="forma.reset();"> </img>
			<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>
		</td>
	</tr>
  </table>
</form>
</div>
</body>
</html>
