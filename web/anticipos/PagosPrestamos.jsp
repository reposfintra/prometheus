<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    Vector anticipos    = model.PrestamoSvc.getAnticipos();
    Vector prestamos    = model.PrestamoSvc.getPrestamos();
	String msg          = (String) request.getAttribute("msg");	
	String beneficiario = (String) request.getParameter("beneficiario");	
	String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
	double total_prestamo = 0;
	double total_anticipo = 0;
	
%>

<html>
<head>
	<title>.: Liquidacion de Prestamos Pendientes</title>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script> 
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script> 
	<script language="JavaScript1.2">
	<!--
	   // Maximizar Ventana por Nick Lowe (nicklowe@ukonline.co.uk)
	
	   window.moveTo(0,0);
	   if (document.all) {
		 top.window.resizeTo(screen.availWidth,screen.availHeight);
	   }
	   else if (document.layers||document.getElementById) {
		   if(top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		   top.window.outerHeight = screen.availHeight;
		   top.window.outerWidth = screen.availWidth;
		   }
	   }
	//-->
	</script>
	<script>
	
		function formato(numero){
			var dt = (numero+'').split('\.');
			var parteEntera  = dt[0]	;
			var tmp = parseFloat(parteEntera) ;
			var factor = (tmp < 0 ? - 1 : 1);
			tmp *= factor;
			var num = '';
			var pos = 0;
			while(tmp>0){
				if (pos%3==0 && pos!=0) num = ',' + num;
				res  = tmp % 10;
				tmp  = parseInt(tmp / 10);
				num  = res + num  ;
				pos++;
			}
			var final = (num!='0' ? (factor==-1 ? '-' : '' ) + num : '') ;
			return ( final=='') ? '0' : final;
		}      

		function sinformato(element){
			return element.replace( new RegExp(",","g") ,'');
		}
		
		function formatear(element){
			element.value = formato(sinformato(element.value));
		}
			
			
		function cambiarAbonos(){
			var t_planillas         = parseFloat(sinformato(formulario.t_planillas.value));
			var t_prestamos         = parseFloat(sinformato(formulario.t_prestamos.value));
			var consignacion        = parseFloat(sinformato(formulario.t_consignacion.value));
			var t_saldo_propietario = parseFloat(sinformato(formulario.t_saldo_propietario.value));
			var t_saldo_prestamo    = parseFloat(sinformato(formulario.t_saldo_prestamo.value));
			
			formulario.t_saldo_propietario.value = (t_planillas-consignacion) ;
			formulario.t_saldo_prestamo.value    = (t_prestamos-consignacion) ;
			
			formatear(formulario.t_saldo_propietario);
			formatear(formulario.t_saldo_prestamo   );			
		}
		
		function _submit(formulario){
			var t_planillas         = parseFloat(sinformato(formulario.t_planillas.value));
			var t_prestamos         = parseFloat(sinformato(formulario.t_prestamos.value));
			var consignacion        = parseFloat(sinformato(formulario.t_consignacion.value));
			var t_saldo_propietario = parseFloat(sinformato(formulario.t_saldo_propietario.value));
			var t_saldo_prestamo    = parseFloat(sinformato(formulario.t_saldo_prestamo.value));
			if ( consignacion <= 10 ){
				alert ('Sea serio cuadro...');								
			} else if ( t_saldo_propietario < 0 ){
 				alert ('Fondo insuficiente del propietario...');
			} else if ( t_saldo_prestamo   < 0 ){
 				alert ('Rectifique el valor de la deuda, esta cancelando de mas...');
			} else {
				formulario.submit();
			}			
		}
	</script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos Pagos Terceros"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

<% if (anticipos!=null && !anticipos.isEmpty() ) { %>
<table width="500" border="2">
<tr>
	<td>
		<table cellpadding='0' cellspacing='0' width='100%'>
			 <tr>
				  <td width="69%" align="left" class="subtitulo1">&nbsp;ANTICIPOS PENDIENTES</td>
				  <td width="31%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>
	   </table>
	
		<table  width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
			<tr class="tblTitulo">
				<th width="33%">PLANILLA</th>
				<th width="33%">PLACA</th>
				<th width="34%">VALOR</th>
			</tr>
			<%  for (int i = 0; i < anticipos.size(); i++ ) { 
				  AnticiposTerceros ant = (AnticiposTerceros) anticipos.get(i);
				  total_anticipo += ant.getVlrNeto();
			%>
			<tr class="<%= (i%2==0?"filagris":"filaazul") %>">
				<td class="bordereporte" align="center">&nbsp;<%= ant.getPlanilla() %></td>
				<td class="bordereporte" align="center">&nbsp;<%= ant.getSupplier() %></td>
				<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat(ant.getVlrNeto())  %>&nbsp;</td>	
			</tr>
			<% } %>
			<tr class="tblTitulo">
				<td class="bordereporte" align="right" colspan="2" >TOTAL PLANILLAS</td>
				<td class="bordereporte" align="right" ><%= UtilFinanzas.customFormat(total_anticipo)  %>&nbsp;</td>
			</tr>
		</table>	
	</td>
</tr>
</table>
<% }  else { %>
	<br><br><span class="informacion">No hay anticipos pendientes para liquidar Prestamos.</span>
<% } %>
<br>



<% if (prestamos!=null && !prestamos.isEmpty() ) { %>
<table width="600" border="2">
<tr>
	<td>
		<table cellpadding='0' cellspacing='0' width='100%'>
			 <tr>
				  <td width="69%" align="left" class="subtitulo1">&nbsp;PRESTAMOS PENDIENTES</td>
				  <td width="31%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>
	   </table>
	
		<table  width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
			<tr class="tblTitulo">
				<th width="25%" >PRESTAMO</th>
				<th width="25%" >ULTIMO SALDO</th>
				<th width="25%" >INTERESES</th>
				<th width="25%" >TOTAL SALDO</th>
			</tr>
			<% for (int i = 0; i < prestamos.size(); i++ ) { 
				Prestamo p = (Prestamo) prestamos.get(i);
				total_prestamo += p.getMonto() + p.getIntereses();
			%>
			<tr class="<%= (i%2==0?"filagris":"filaazul") %>">
				<td class="bordereporte" align="center">&nbsp;<%= p.getId() %></td>
				<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat(p.getMonto())  %>&nbsp;</td>	
				<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat(p.getIntereses())  %>&nbsp;</td>	
				<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat(p.getMonto() + p.getIntereses())  %>&nbsp;</td>	
			</tr>
			<% } %>
			<tr class="tblTitulo">
				<td class="bordereporte" align="right" colspan="3">TOTAL PRESTAMOS</td>
				<td class="bordereporte" align="right" ><%= UtilFinanzas.customFormat(total_prestamo)  %>&nbsp;</td>
			</tr>
		</table>	
	</td>
</tr>
</table>
<% }  else { %>
	<br><br><span class="informacion">No hay prestamos pendientes para liquidar.</span>
<% } %>
<br><br>




<% // VALORES DE LA LIQUIDACION
   double dif                = total_anticipo - total_prestamo;
   double valor_consignacion = (dif<0? total_anticipo : total_prestamo ) ;
   double saldo_propietario  = total_anticipo - valor_consignacion;
   double saldo_prestamos    = total_prestamo - valor_consignacion;
   if ( anticipos!=null && !anticipos.isEmpty() && prestamos!=null && !prestamos.isEmpty() ) { 
%>
<form action="<%= CONTROLLER %>?estado=Liquidacion&accion=Prestamos&opcion=LIQUIDAR" method="post" name="formulario">
<table width="400" border="2">
<tr>
	<td>
		<table cellpadding='0' cellspacing='0' width='100%'>
			 <tr class="subtitulo1">
				  <td width="69%" align="left" >&nbsp;LIQUIDACION DE LOS PRESTAMOS</td>
				  <td width="31%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>
	   </table>
	
		<table  width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
			<tr class="tblTitulo">
				<td width="48%" class="bordereporte">TOTAL PLANILLAS</td>
				<td width="52%" class="bordereporte" align="right"><input type="text" name="t_planillas" style="text-align:right; width:97%; border:0" readonly value="<%= UtilFinanzas.customFormat( total_anticipo )  %>"></td>				
			</tr>
			<tr class="tblTitulo">
				<td width="48%" class="bordereporte">TOTAL PRESTAMOS</td>
				<td width="52%" class="bordereporte" align="right"><input type="text" name="t_prestamos" style="text-align:right; width:97%; border:0; " readonly value="<%= UtilFinanzas.customFormat( total_prestamo )  %>"></td>				
			</tr>
			<tr class="tblTitulo">
				<td width="48%" class="bordereporte">VALOR A CANCELAR</td>
				<td width="52%" class="bordereporte" align="right"><input type="text" name="t_consignacion" style="text-align:right; width:97%; border:0; color:#003333; font-weight:bold" value="<%= UtilFinanzas.customFormat( valor_consignacion )  %>" onFocus="this.select();"  onKeyPress="soloDigitos(event, 'decNO')" onChange="formatear(this); cambiarAbonos(); "></td>				
			</tr>
			<tr class="tblTitulo">
				<td width="48%" class="bordereporte">SALDO PROPIETARIO</td>
				<td width="52%" class="bordereporte" align="right"><input type="text" name="t_saldo_propietario" style="text-align:right; width:97%; border:0; color:#003333; font-weight:bold" readonly value="<%= UtilFinanzas.customFormat( saldo_propietario )  %>"></td>				
			</tr>
			<tr class="tblTitulo">
				<td width="48%" class="bordereporte">SALDO PRESTAMOS</td>
				<td width="52%" class="bordereporte" align="right"><input type="text" name="t_saldo_prestamo" style="text-align:right; width:97%; border:0; color:#003333; font-weight:bold" readonly value="<%= UtilFinanzas.customFormat( saldo_prestamos )  %>"></td>				
			</tr>
		</table>	
	</td>
</tr>
</table>
<br>
<br>
 <img src="<%=BASEURL%>/images/botones/aceptar.gif"       height="21"  title='Aceptar'    onclick='_submit(formulario)'                                                                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 <img src="<%=BASEURL%>/images/botones/salir.gif"         height="21"  title='Salir'      onClick="parent.close();"                                                                                          onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 <input type="hidden" name="beneficiario" value="<%= beneficiario %>">
</form>

<% } else { %>     
	<br><span class="informacion">No hay datos para liquidar.</span>    
<% } %>

<% if(msg!=null  &&  !msg.equals("") ){%>
	<BR><BR>
	<table border="2" align="center">
		  <tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					  <tr>
							<td width="450" align="center" class="mensajes"><%= msg %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp; </td>
					  </tr>
				 </table>
			</td>
		  </tr>
	</table>
<%}%>		  
</center>
</div>
<%=datos[1]%> 
</body>
</html>
