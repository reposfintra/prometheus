<!--
- Autor      : Ing. Fernell Villacob
- Date       : 05  Agosto 2006
- Copyrigth Notice : Transporte Sanchez Polo S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite escojer banco para la transferencia
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>



<html>
<head>
     <title>Lista de Bancos</title>
     <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	 <link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/transferencias.js"></script>
     
     

 <% 
    String  nit       = request.getParameter("nit");
    String  nombre    = request.getParameter("nombre");
    String  anticipo  = request.getParameter("anticipo"); 
    String  global    =  request.getParameter("global");
	String  secue =  request.getParameter("secu");
	String  desBanco  = request.getParameter("infoCTA"); 
    String  URL       = CONTROLLER+"?estado=Anticipos&accion=PagosTerceros&evento=ASIGNARBANCO_TRANSACCION&nit="+ nit +"&anticipo="+ anticipo +"&global="+ global +"&secue="+secue;
	List bancosTercero =  model.AnticiposPagosTercerosSvc.getListCTATercero();
    String  sendPP    = request.getParameter("sendPP");
	boolean existePrestamo = model.PrestamoSvc.existePrestamosTipoAbonos(nit);
%>
    
    
     
     <script>
           function  asignar(theForm){   
             <% if( bancosTercero.size()>0){%> 
                  var sec = '';
                  for(var i=0;i<theForm.length;i++)
                      if(theForm.elements[i].checked )
                           sec = theForm.elements[i].value;                       
                  window.close();
                  parent.opener.location.href = '<%=URL%>&infoCTA='+ sec;
				  
             <%}else{%>
                 alert('No presenta cuentas para pago');
             <%}%>
           }
    </script>

     
     
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>
    
    
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Lista de Bancos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
  

    

  <form action="" method='post' name='formulario' >
 
   <table width="98%" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                      <tr class="barratitulo">
                        <td colspan='2' >
                           <table cellpadding='0' cellspacing='0' width='100%'>
                             <tr>
                              <td align="left" width='65%' class="subtitulo1">&nbsp;CUENTAS PROVEEDOR ANTICIPO <br> </td>
                              <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                            </tr>
                           </table>
                        </td>
                     </tr>
                     
                     <tr class="fila">
                            <td width='100%' colspan='2'>
                                <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">

                                     <TR  class="tblTitulo"  >      
                                            <TH nowrap style="font size:11; font weight: bold">&nbsp      </TH>
                                            <TH nowrap style="font size:11; font weight: bold">Banco     </TH>
                                      </TR>

										<%
										//'comision(formulario) '<select name='infoCTA' style='width:30%' onChange='' >
										
										if( bancosTercero.size()>0){
											for(int i=0;i<bancosTercero.size();i++){
											     
												 String     sec   = "";
                                                 if(i==0)
												    sec   = "S";
												 else
												 	sec   = "";
                                                 String     sel   = (sec.equals("S"))?"checked":"";
												
												 Hashtable  bk            = (Hashtable)bancosTercero.get(i);
												 String     aliasBK       = (String) bk.get("codigo");
												 String     descripcion   = (String) bk.get("descripcion");
												 String     vec[]         =  descripcion.split(",");

												 if( vec.length== 5){
													 String codeBK  = vec[0];
													 String descBK  = vec[1];
													 String ctaBK   = vec[2];
													 String tipoCTA = vec[3];
													 
													 if (i==0) { %>
												 	<script> 	
																var codeBK_1 = '<%=codeBK%>' ; 
																var descBK_1 = '<%=descBK%>' ;  
																var ctaBK_1  = '<%=ctaBK%>' ;  
																var tipoCTA_1= '<%=tipoCTA%>' ; 
													</script> 
												 <%}
													 
													 
													  %>
                                              <tr class='<%= (i%2==0?"filagris":"filaazul") %>'   style=" font size:12" onMouseOver='cambiarColorMouse(this)'  >         

                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><input type='radio' <%= sel %>  name='CTA' id = 'CTA<%=i%>' value='<%=codeBK%>-<%=descBK%>-<%=ctaBK%>-<%=tipoCTA%>' onchange=" if(this.checked) { codeBK_1 = '<%= codeBK %>'; descBK_1 = '<%= descBK %>';  ctaBK_1 = '<%= ctaBK %>';  tipoCTA_1 = '<%= tipoCTA %>'; } " ></TD>
                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><%=aliasBK%> - <%=ctaBK%>  - <%=tipoCTA%>  </TD>
                                             </tr>
                                                <%} 
										    }
                                        }else{%>               
                                           <TR><Th colspan='7' class="bordereporte" align='center' nowrap style="font size:11" > No presenta cuenta... </Th></TR>
                                     <%}%>

                                 </table>
                           </td>
                       </tr> 
         
              </table>
         </td>
      </tr>
   </table> 
  
   </form>
   
   
   <br>
                 
   <img src="<%=BASEURL%>/images/botones/aplicar.gif"    height="21"  title='Aplicar'    onclick='<% if (sendPP==null) { %> asignar(formulario); <% } else { %> retornarParametrosPago2(codeBK_1,descBK_1, ctaBK_1 , tipoCTA_1); <% } %>' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   
   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"                                                                                             onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   

</div>
<%=datos[1]%>  

</body>
</html>
<script>
	<% if (existePrestamo) { %>
		alert('El proveedor tiene prestamos pendentes por cancelar.');
	<% } %>
</script>