<!--
- Autor      : Ing. Fernell Villacob
- Date       : 04  Agosto 2006
- Copyrigth Notice : Transporte Sanchez Polo S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite filtrar datos para consulta de anticipos pagos terceros
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
<head>
        <title>Anticipos Pagos Terceros</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script> 
	
	<script>
	   function send(theForm){
	            
	           var sw = 0;
	           for(var i=0;i<theForm.length;i++){
                      var ele = theForm.elements[i];
                      if(ele.type=='checkbox' && ele.checked  ){
                         if( ele.name=='ckAgencia')      {  if( theForm.Agencia.value     ==''){  alert('Deber� seleccionar la agencia');         sw=1; theForm.Agencia.focus();      break;  }  }
                         if( ele.name=='ckPropietario')  {  if( theForm.Propietario.value ==''){  alert('Deber� establecer nit del propietario'); sw=1; theForm.Propietario.focus();  break;  }  } 
                         if( ele.name=='ckPlanilla')     {  if( theForm.Planilla.value    ==''){  alert('Deber� establecer la planilla');         sw=1; theForm.Planilla.focus();     break;  }  }
                         if( ele.name=='ckPlaca')        {  if( theForm.Placa.value       ==''){  alert('Deber� establecer la placa');            sw=1; theForm.Placa.focus();        break;  }  }                      
                         if( ele.name=='ckConductor')    {  if( theForm.Conductor.value   ==''){  alert('Deber� establecer nit del Conductor');   sw=1; theForm.Conductor.focus();    break;  }  } 
                         
                      }
                   }	   
                   if(sw==0)
	              theForm.submit();
	     
	   }
	</script>
	
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos Pagos Terceros"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
  
   <% List agencias =  model.AnticiposPagosTercerosSvc.getListAgencias(); %>

   <form action="<%=CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=BUSCARPORAPROBAR" method='post' name='formulario' >
 
   <table width="450" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;CONSULTA ANTICIPOS - FILTROS</td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>
                 

                 <tr  class="fila">
                       <td colspan='2' >
                       
                          <table class='tablaInferior' width='100%'>
                          
                               <tr  class="fila">
                                   <td width='7%'  > <input type='checkbox'   name='ckAgencia'>    </td>
                                   <td width='23%' > Agencia                                       </td>
                                   <td width='*'   >            
                                                     <select name='Agencia'>
                                                         <option value=''></option>
                                                         <% for(int i=0;i<agencias.size();i++){
                                                                Hashtable  agencia = (Hashtable)agencias.get(i);%>
                                                                <option value='<%=agencia.get("codigo") %>'>  <%=agencia.get("nombre") %>  </option>
                                                         <%}%>   
                                                     </select>
                                   </td>
                               </tr>
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckPropietario'>  </td>
                                   <td             > Propietario                                     </td>
                                   <td             > <input type='text' name='Propietario' title='Nit del Propietario' maxlength='15'></td>
                               </tr>
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckConductor'>  </td>
                                   <td             > Conductor                                      </td>
                                   <td             > <input type='text' name='Conductor' title='Nit del Conductor' maxlength='15'></td>
                               </tr>
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckPlaca'>      </td>
                                   <td             > Placa                                         </td>
                                   <td             > <input type='text' name='Placa' title='N�mero de Placa' maxlength='6'>       </td>
                               </tr>
                               
                               <tr  class="fila">
                                   <td             > <input type='checkbox'   name='ckPlanilla'>   </td>
                                   <td             > Planilla                                      </td>
                                   <td             > <input type='text' name='Planilla' title='N�mero de planilla' maxlength='6'> </td>
                               </tr>
                               
                          </table>
                       
                       </td>
                 </tr>
                 
                 

           </table>
         </td>
      </tr>
   </table> 
  
   <br>                 
   <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Consultar'  onclick='send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   
   
   
</div>
<%=datos[1]%>  


</body>
</html>
