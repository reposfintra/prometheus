<!--
- Autor      : Ing. Fernell Villacob
- Date       : 04  Agosto 2006
- Copyrigth Notice : Transporte Sanchez Polo S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite mostrar anticipos pagos terceros
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<html>
<head  >

        <title>Listado Anticipos Pagos Terceros</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>

                <!--inicio de 20101014-->
		<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
		<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
		<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
		<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
                <link href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" type="text/css" rel="stylesheet" />
                <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
                <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
                <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
                <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>

		<!--fin de 20101014-->
 <%  Usuario usuario = (Usuario) session.getAttribute("Usuario");
     String perfil = modelopav.ClientesVerService.getPerfil(usuario.getLogin());
     String  vista  = request.getParameter("vista");
     String  msj    = request.getParameter("msj");
     String  title  = ( vista.equals("APROBAR") )?"APROBAR ANTICIPOS - ":"TRANSFERIR ANTICIPOS - ";
     String  opcion = ( vista.equals("APROBAR") )?"APROBAR":"TRANSFERIR";
     String  colums = ( vista.equals("APROBAR") )?"13":"30";
     String  URL    = CONTROLLER +"?estado=Anticipos&accion=PagosTerceros&evento="+opcion;
     List    lista  = (List) request.getAttribute("listaAnticipos");
	 String  Banco  = request.getParameter("desBanco");
	 String  Ncuenta = request.getParameter("Ncuenta");

         List bancosTercero_g =  model.AnticiposPagosTercerosSvc.getListCTATercero();

	 %>

         <script type="text/javascript">
            function send(theForm){
               var con=0;
             <% if( vista.equals("TRANSFERIR") ){%>
                      if(theForm.infoCTA.value===''){
                         alert('Deberá establecer la cuenta del proveedor del anticipo');
                         theForm.infoCTA.focus();
                         con=1;
                      }
             <%}%>
                if(con===0){
                   for(var i=0;i<theForm.length;i++){
                       var ele = theForm.elements[i];
                       if(ele.type==='checkbox'  &&  ele.id!=='All'  && ele.checked  )
                          con++;
                   }
                   if(con===0){ 
                       alert('Deberá seleccionar por lo menos un anticipo');
                   }else{
                       divEspera("Espere un momento por favor...", "270", "130");
                       theForm.submit();
                   }    
                }
            }

			function comision (form){
				form.action =  "<%=CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=BUSCARAPROBADAS";
				form.submit();
			}

			function Confirm(form,secuencia){
				if( confirm('Desea Reversar la Transaccion?') ){
					//alert('values  '+secuencia);
					form.action =  "<%=CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=ELIMINARTRANSACCION&sec="+secuencia;
					//alert('al submit');
					form.submit();
				}
			}


			function sendToPrestamo(){

				if (formulario.propietario_p.value!='') {
					var url = "<%= CONTROLLER %>?accion=Prestamos&estado=Liquidacion&opcion=LoadPago&beneficiario="+formulario.propietario_p.value;
					var w = window.open( url ,'PagosPrestamos','menubar=no, status=yes, rezisable, scrollbar=yes');
					w.focus();
				} else {
					alert('Debe seleccionar un propietario para poder continuar.');
				}
			}
		//rhonalf
                var misfilas = 0;
                function sendit(){
                    var action = '<%=CONTROLLER %>?estado=Anticipos&accion=PagosTerceros&evento=';
                    document.getElementById("formulario").action = action + 'modbanco&misfilas='+misfilas;
                    document.getElementById("formulario").submit();
                }
                //rhonalf
                function deselect(){
                    document.getElementById("aceptarimg").src='<%=BASEURL%>/images/botones/aceptarDisable.gif';
                    document.getElementById("aceptarimg").onclick=function alerta(){alert('Boton deshabilitado...');};
                    document.getElementById("aceptarimg").onmouseover='';
                    document.getElementById("aceptarimg").onmouseout=function redest(){document.getElementById("aceptarimg").src='<%=BASEURL%>/images/botones/aceptarDisable.gif';};
                }



				/**********************************************************/
			 function selec_banco(n)
			 {
				  var index=document.getElementById('bank_g').selectedIndex;
				  var chek=true;
				 for (i=0;i<n;i++)
				 {
							 c="chx_b"+i;
							 b="bank"+i;
							if(document.getElementById(c).checked==true)
							{
								document.getElementById(b).selectedIndex=index;
							}

				 }
          }


            <%if(lista!=null)
                {%>

		function aReload()
		{


                         var sw=true; var c=0;


                   if ('<%=lista.size()%>'!='0')
                   {


		  var no_items = formulario.anticipo.length;

		for (i=0;i<no_items;i++)
		{
			if(formulario.anticipo[i].checked==true)
			{
			c=c+1;
			}
		}


               }

				  if(c==0)
				  {

					  location.href="<%=CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=REFRESCAR&vista=<%=vista%>";
				  }
				  else
				  {
					 startReload()
				  }
         }


		function startReload()
		{
		     if ('APROBAR'=='<%=vista%>')
                     {
			setTimeout("aReload()", 50000);
                     }
		}

		<%}%>
                
function divEspera(msj, width, height) {

    $("#msj2").html(msj);
    $("#dialogo2").dialog({
        width: width,
        height: height,
        show: "scale",
        hide: "scale",
        resizable: false,
        position: "center",
        modal: true,
        closeOnEscape: false
    });

    $("#dialogo2").siblings('div.ui-dialog-titlebar').remove();

}      
	// -->
</script>

<style>
.ventana{
    display:none;    
    font-family:Arial, Helvetica, sans-serif;
    color:#808080;
    font-size:12px;
    text-align:justify;

}
    
</style>

</head>
<body onLoad="redimensionar();startReload()" onResize="redimensionar();">



<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos Pagos Terceros"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>




        <%String  semirepetidos=model.AnticiposPagosTercerosSvc.obtainAnticiposSemiRepetidos(lista);//20101013 se hallan los semirepetidos para la lista%>

        <form action="<%=URL%>" method='post' name='formulario' id='formulario' >


           <table width="98%" border="2" align="center">
               <tr>
                  <td>
                       <table width='100%' align='center' class='tablaInferior'>

                          <tr class="barratitulo">
                            <td colspan='<%= colums %>' >
                               <table cellpadding='0' cellspacing='0' width='100%'>
                                     <tr>
                                          <td align="left" width='70%' class="subtitulo1">&nbsp;<%=title%>  [ <%=  model.AnticiposPagosTercerosSvc.getProveedor() +" "+ model.AnticiposPagosTercerosSvc.getNameProveedor()%> ]</td>
                                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
                                    </tr>
                               </table>
                            </td>
                         </tr>


                         <tr class="fila">
                            <td width='100%' colspan='2'>
                                <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">

								<% if( vista.equals("TRANSFERIR") ){
										List bancosTercero =  model.AnticiposPagosTercerosSvc.getListCTATercero();
										TreeMap lista_propietarios =  model.AnticiposPagosTercerosSvc.getPropietariosConPrestamos();
								%>

										<tr  class="fila">
										   <td colspan='<%= colums %>'>
													<table  width='100%' bordercolor="#999999" bgcolor="#F7F5F4" align="center">
														  <tr  class="fila">
															   <td width='15%'  > CUENTAS PROVEEDOR ANTICIPO      </td>

															   <td width='12%' >
																	<select name='infoCTA'  onChange='comision(formulario)' >
																	   <%

																	   if( bancosTercero.size()>0){
																		   for(int i=0;i<bancosTercero.size();i++){
																				 Hashtable  bk            = (Hashtable)bancosTercero.get(i);

																				 String     aliasBK       = (String) bk.get("codigo");
																				 String     descripcion   = (String) bk.get("descripcion");
																				 String     vec[]         =  descripcion.split(",");
																				 if( vec.length== 5){
																					 String codeBK  = vec[0];
																					 String descBK  = vec[1];
																					 String ctaBK   = vec[2];
																					 String tipoCTA = vec[3];
																					 //System.out.println("a if   "+ctaBK+"  "+Ncuenta); %>

																					 <%if(Ncuenta != null && Ncuenta.equals(ctaBK) ){%>
																						   <option selected value='<%=codeBK%>-<%=descBK%>-<%=ctaBK%>-<%=tipoCTA%>'> <%=aliasBK%> - <%=ctaBK%>  - <%=tipoCTA%>  </option>
																					 <%}else{
																							if ( Ncuenta == null && i==0){
																							     Ncuenta = ctaBK;
																							}%>
																								<option value='<%=codeBK%>-<%=descBK%>-<%=ctaBK%>-<%=tipoCTA%>'> <%=aliasBK%> - <%=ctaBK%>  - <%=tipoCTA%>  </option>

																					 <%}%>
																			  <%}else{%>
																					 <option value=''> <%=aliasBK%> - Información de Cuenta Incompleta </option>
																				<%}
																			}
																		 }else{%>
																			   <option value=''> No presenta información de cuentas registrada </option>
																	   <%}%>

																	</select>
															   </td>
                                                                                                                            <td width='12%'> TOTAL CONSIGNADO      </td>
                                                                                                                            <td >$ <input id="txtValorSum" type='text' style="font-size: 14px;color: rgb(0, 51, 153);font-weight: bold;" value="0" readOnly />   </td>
														  </tr>

														  <% if (  Ncuenta != null && Ncuenta.equals("FINTRA")) { %>
														  <tr class="fila">
														  	<td>PROPIETARIO DE PRESTAMOS</td>
															<td><input:select name="propietario_p" options="<%= lista_propietarios %>" attributesText="class='textbox' id='propietario_p' style='width:300' onchange='comision(formulario);' " default="" /></td>
														  </tr>
														  <% } %>
													</table>
											</td>
										</tr>

                                        <%}%>


                                         <% if( lista !=null && lista.size()>0){%>


                                         <tr class="tblTitulo" >
                                              <TH   nowrap style="font-size:11px; font-weight: bold" height='25'       colspan='<%=(vista.equals("TRANSFERIR"))?"13":"15"%>' >DATOS ANTICIPO        </TH>
                                              <% if( vista.equals("TRANSFERIR") ){%>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" colspan='5' >VALORES               </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" colspan='1' >&nbsp;</TH>

                                                    <TH   nowrap style="font-size:11px; font-weight: bold" colspan='9' >CUENTA A TRANSFERIR   </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" colspan='2' >APROBACION             </TH>


                                              <%}%>
                                         </tr>


                                         <tr class="tblTitulo" >
					 <% if(!vista.equals("TRANSFERIR")){%>
										 			<!-- Julio Barros 17-11-2006 cambio de pocision-->
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" > <input type='checkbox'  id='All' onClick="Sell_all(this.form,this);">  </TH>
					 <%}%>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >No                </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >AGENCIA           </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >CONDUCTOR         </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >PROPIETARIO       </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >PLACA             </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >PLANILLA          </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >FECHA             </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >REANT             </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >VALOR             </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >ASESOR             </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >REFERENCIADO       </TH>
                                              <TH   nowrap style="font-size:11px; font-weight: bold" >USER_CREACION      </TH>
                                               <% if(  vista.equals("APROBAR")) {%>
                                                          <th   nowrap style="font-size:11px; font-weight: bold" >FECHA_CREACION </th>
                                                 <%}%>
											  <th   nowrap style="font-size:11px; font-weight: bold" >DES_CONCEPT </th>

                                              <% if( vista.equals("TRANSFERIR") ){%>

                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >% DESC            </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >VLR DESC          </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >VLR NETO          </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >COMISION          </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >CONSIGNAR         </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >
                                                    <span >
                                                    <input type='checkbox'  id='All_b' onClick="Sell_all_b(this.form,<%=lista.size()%>,'b');" onChange="selec_banco(<%=lista.size()%>);" ></span>
                                                    <select  id="bank_g" name="bank_g" onChange="selec_banco(<%=lista.size()%>);">
                                                      <option value="..." selected >Seleccione</option>
                                                      <%
						 for(int x=0;x<bancosTercero_g.size();x++){
                                                     Hashtable  b           = (Hashtable)bancosTercero_g.get(x);
                                                    %>
                                                      <option value="..."><%= (String) b.get("codigo")%></option>
                                                      <%}%>
                                                    </select>
                                                    CUENTA GIRADORA  </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >LQ</TH>

                                                    <!-- Julio Barros 17-11-2006 cambio de pocision-->
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" > <input type='checkbox'  id='All' onClick="Sell_all(this.form,this);Totalizar_Select2(this.form,<%=lista.size()%>)">  </TH>

                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >BANCO             </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >SUCURSAL          </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >CUENTA            </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >TIPO CTA          </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >NOMBRE CTA        </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >NIT    CTA        </TH>


                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >FECHA             </TH>
                                                    <TH   nowrap style="font-size:11px; font-weight: bold" >USUARIO           </TH>
													<TH   nowrap style="font-size:11px; font-weight: bold" >ELIMINAR           </TH>

                                              <%}%>

                                         </tr>


                                         <%for(int i=0;i<lista.size();i++){
                                                 AnticiposTerceros anticipo = (AnticiposTerceros) lista.get(i);%>
                                                         <% if(  vista.equals("APROBAR")  || (vista.equals("TRANSFERIR")  ) ){%>
                                                           <tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>'   style=" font-size:12"   >

                                                         <% if(!vista.equals("TRANSFERIR") ){%>
                                                         <!-- Julio Barros 17-11-2006 cambio de pocision-->
                                                              <td class="bordereporte" align='center' nowrap style="font-size:11px">
                                                                     <input type='checkbox' name='anticipo'  id="anticipo" value='<%= anticipo.getId()%>' onClick=" cambiarColorMouse(fila<%=i%>); Sell_all(this.form,this);">
                                                              </td>
                                                         <%}%>
                                                                 <td class="bordereporte"                nowrap style="font-size:10px"> <%=  i+1           %> </td>
                                                                 <td class="bordereporte"                nowrap style="font-size:10px"> <%=  anticipo.getNombreAgencia()           %> </td>
                                                                 <td class="bordereporte"                nowrap style="font-size:10px"> <%=  anticipo.getConductor()               %> - <%=  anticipo.getNombreConductor()         %> </td>
                                                                 <td class="bordereporte"                nowrap style="font-size:10px" onclick= "javascript: var x= window.open( '<%= CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=LISTABANCOS&nit=<%=anticipo.getConductor()%>&anticipo=<%= anticipo.getId()%>&global=S&Ncuenta=<%=Ncuenta%>&infoCTA=<%=Banco%>','Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes  ');  x.focus();" >&nbsp
                                                                 <%=  anticipo.getPla_owner()               %> - <%=  anticipo.getNombrePropietario()       %> </td>
                                                                 <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%=  anticipo.getSupplier()                %> </td>
																 <td class="bordereporte" align='center' nowrap style="font-size:9" >
                                                                                                                                     <%if (model.AnticiposPagosTercerosSvc.esAnticipoSemiRepetido(""+anticipo.getId()+"").equals("S")){  %><img width='14' title='semirepetido' style="cursor:hand" src='<%=BASEURL%>/images/alert-icon.jpg'>&nbsp; <%  }//20101013 si es semirepetido se pone color rojo%>
																 <% if( anticipo.getPlanilla().equals("E"+anticipo.getSecuencia()) ){System.out.println("fecha "+anticipo.getFecha_anticipo());%>
																 		<img width='14' title='Consultar Pronto pago' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/buscar.gif'
																		onclick="javascript: var x= window.open( '<%= CONTROLLER%>?estado=Extracto&accion=PP&opcion=ExtractoLiquidacion&nit=<%= anticipo.getPla_owner()%>&fecha=<%=anticipo.getFecha_anticipo() %>',
																		'Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes');  x.focus();">&nbsp
																		<%= anticipo.getPlanilla()  %>
																 <% }else{%>
                                                                 		<%= anticipo.getPlanilla()                %>
																 <%}%>
																 </td>
                                                                 <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%=  anticipo.getFecha_anticipo()          %> </td>
                                                                 <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%=  anticipo.getReanticipo()              %> </td>
                                                                 <td class="bordereporte" align='right'  nowrap style="font-size:10px"> <%=  Util.customFormat(anticipo.getVlr() ) %> </td>
                                                                 <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%=  anticipo.getAsesor()              %> </td>
                                                                 <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%=  anticipo.getReferenciado()        %> </td>
                                                                 <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%=  anticipo.getUsuario_creacion()    %> </td>
                                                                 <%if(  vista.equals("APROBAR")) {%>
                                                                     <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%= anticipo.getFecha_creacion()  %> </td>
                                                                 <%}%>
																 <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%= anticipo.getDes_concept()    %> </td>
                                                         <%}%>



                                                         <% if( vista.equals("TRANSFERIR")    ){%>
                                                             <!-- Valores -->
                                                              <td class="bordereporte" align='center' nowrap>
                                                                       <input type='text' style="font-size:10px; width:30"  name='porcentaje<%=i%>' id='porcentaje<%=i%>'    dir='rtl'        value='<%=  anticipo.getPorcentaje()    %>' onfocus='this.select()' maxlength='4' >
                                                                       <!--% if( anticipo.getReanticipo().equals("S") ){%-->
                                                                             <img width='14'    title='Aplicar descuento' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/modificar.gif'
                                                                             onclick="location.href = '<%=CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=ASIGNARDESCUENTO&nit=<%=anticipo.getPla_owner()%>&anticipo=<%= anticipo.getId()%>&infoCTA=<%=Banco%>&Ncuenta=<%=anticipo.getCuenta_transferencia()%>&valor=' + porcentaje<%=i%>.value; ">
                                                                       <!--%}%-->
                                                              </td>
                                                              <td class="bordereporte" align='center' nowrap> <input type='text' style="font-size:10px; width:70; text-align:right"  name='vlrDescuento<%=i%>' id='vlrDescuento<%=i%>' readonly value='<%= Util.customFormat( anticipo.getVlrDescuento() ) %>'> </td>
                                                              <td class="bordereporte" align='center' nowrap> <input type='text' style="font-size:10px; width:80; text-align:right"  name='vlrNeto<%=i%>'      id='vlrNeto<%=i%>'   readonly value='<%= Util.customFormat( anticipo.getVlrNeto()      ) %>'> </td>
                                                              <td class="bordereporte" align='center' nowrap> <input type='text' style="font-size:10px; width:80; text-align:right"  name='vleComision<%=i%>'  id='vleComision<%=i%>'   readonly value='<%= Util.customFormat( anticipo.getVlrComision()  ) %>'> </td>
                                                              <td class="bordereporte" align='center' nowrap> <input type='text' style="font-size:10px; width:80; text-align:right"  name='vlrConsignar<%=i%>' id='vlrConsignar<%=i%>'  readonly value='<%= Util.customFormat( anticipo.getVlrConsignar() ) %>'> </td>
                                                           <%
                                                                  List bancosTercero =  model.AnticiposPagosTercerosSvc.getListCTATercero();
                                                                  int centinel = 0;
                                                                  if( bancosTercero.size()>0){
                                                                    for(int m=0;m<bancosTercero.size();m++){
                                                                        Hashtable  bk            = (Hashtable)bancosTercero.get(m);
                                                                        String     aliasBK       = (String) bk.get("codigo");
                                                                        String     descripcion   = (String) bk.get("descripcion");
                                                                        String     vec[]         =  descripcion.split(",");
                                                                        if( vec.length == 5){
                                                                                String codeBK  = vec[0];
                                                                                String descBK  = vec[1];
                                                                                String ctaBK   = vec[2];
                                                                                String tipoCTA = vec[3];
                                                                                //System.out.println( "para 1   "+vec[0] );
                                                                                //System.out.println( "para 2   "+anticipo.getBanco_transferencia()+"   descBK  "+descBK );
                                                                                if( anticipo.getBanco_transferencia().equals(codeBK)  ){%>
                                                                                           <td class="bordereporte"  nowrap style="font-size:11px">
                                                                                               <!-- rhonalf -->
                                                                                               <script type="text/javascript">
                                                                                                   misfilas = misfilas + 1;
                                                                                               </script>
                                                                                               <input type="checkbox" id="chx_b<%=i%>" name="chx_b<%=i%>" value="<%=anticipo.getId()%>" onClick="deselect();">
                                                                                               <select id="bank<%=i%>" name="bank<%=i%>">
                                                                                               <option value="">...</option>
                                                                                               <%
                                                                                                    Hashtable  bkx = null;
                                                                                                    AnticiposPagosTercerosServices serv = new AnticiposPagosTercerosServices(usuario.getBd());
                                                                                                    serv.searchListaCuentasTercero(anticipo.getProveedor_anticipo());
                                                                                                    List bancosTercerox = serv.getListCTATercero();
                                                                                                    //System.out.println("agt "+anticipo.getConductor());
                                                                                                    for(int k=0;k<bancosTercerox.size();k++){
                                                                                                        //value=codebk
                                                                                                        bkx= (Hashtable)bancosTercerox.get(k);
                                                                                                        String aliasBKx = (String) bkx.get("codigo");
                                                                                                        String descripcionx = (String) bkx.get("descripcion");
                                                                                                        String vecx[] = descripcionx.split(",");
                                                                                                        String codeBKx = vecx[0];
                                                                                                        String descBKx = vecx[1];
                                                                                                        String ctaBKx = vecx[2];
                                                                                                        String tipoCTAx = vecx[3];
                                                                                                        //System.out.println("agt "+anticipo.getConductor());
                                                                                                        //System.out.println("idant "+anticipo.getId()+" code "+codeBKx+" desc "+descBKx+" cta "+ctaBKx+" tipocta "+tipoCTAx);
                                                                                                %>
                                                                                                <option value="<%=ctaBKx+";_;"+codeBKx+";_;"+tipoCTAx%>"<%if( anticipo.getBanco_transferencia().equals(codeBKx) && anticipo.getCuenta_transferencia().equals(ctaBKx)  ) {out.print(" selected='selected'");}%>><%=aliasBKx %></option>
                                                                                                <%
                                                                                                    }
                                                                                               %>
                                                                                               </select>
                                                                                               <!-- /rhonalf -->

                                                                                               <!-- <img width='14' title='Asignar bancos' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/buscar.gif' onclick= "javascript: var x= window.open( '<%= CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=LISTABANCOS_TRANSACCION&nit=<%=anticipo.getConductor()%>&anticipo=<%= anticipo.getId()%>&global=N&infoCTA=<%=descBK%>&Ncuenta=<%=Ncuenta%>&secue='+ secuencia<%=i%>.value,'Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes  ');  x.focus();" >&nbsp --><%= descBK %>

                                                                                           </td>

                                                                          <%     Banco = descBK;
                                                                                 centinel++;
                                                                                 m=bancosTercero.size()-1;%>

                                                                                <%
                                                                                           //m=bancosTercero.size()-1;
                                                                                }
                                                                    }
                                                            }
                                                                if (centinel < 1 ){%>

                                                                        <td class="bordereporte"  nowrap style="font-size:11px">
                                                                            <!-- rhonalf -->
                                                                       <script type="text/javascript">
                                                                           misfilas = misfilas + 1;
                                                                       </script>
                                                                       <input type="checkbox" id="chx_b<%=i%>" name="chx_b<%=i%>" value="<%=anticipo.getId()%>" onClick="deselect();">
                                                                       <select id="bank<%=i%>" name="bank<%=i%>">
                                                                       <option value="">...</option>
                                                                       <%
                                                                            Hashtable  bkx = null;
                                                                            AnticiposPagosTercerosServices serv = new AnticiposPagosTercerosServices(usuario.getBd());
                                                                            serv.searchListaCuentasTercero("802022016");
                                                                            List bancosTercerox = serv.getListCTATercero();
                                                                            //System.out.println("agt "+anticipo.getConductor());
                                                                            for(int k=0;k<bancosTercerox.size();k++){
                                                                                //value=codebk
                                                                                bkx= (Hashtable)bancosTercerox.get(k);
                                                                                String aliasBKx = (String) bkx.get("codigo");
                                                                                String descripcionx = (String) bkx.get("descripcion");
                                                                                String vecx[] = descripcionx.split(",");
                                                                                String codeBKx = vecx[0];
                                                                                String descBKx = vecx[1];
                                                                                String ctaBKx = vecx[2];
                                                                                String tipoCTAx = vecx[3];
                                                                                //System.out.println("agt "+anticipo.getConductor());
                                                                                //System.out.println("idant "+anticipo.getId()+" code "+codeBKx+" desc "+descBKx+" cta "+ctaBKx+" tipocta "+tipoCTAx);
                                                                        %>
                                                                        <option value="<%=ctaBKx+";_;"+codeBKx+";_;"+tipoCTAx%>"<%if( anticipo.getBanco_transferencia().equals(codeBKx) && anticipo.getCuenta_transferencia().equals(ctaBKx)  ) {out.print(" selected='selected'");}%>><%=aliasBKx %></option>
                                                                        <%
                                                                            }
                                                                       %>
                                                                       </select>
                                                                       <!-- /rhonalf -->
                                                                       <!--
                                                                            <img width='14' title='Asignar bancos' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/buscar.gif' onclick= "javascript: var x= window.open( '<%= CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=LISTABANCOS_TRANSACCION&nit=<%=anticipo.getConductor()%>&anticipo=<%= anticipo.getId()%>&global=N&infoCTA=<%=""%>&Ncuenta=<%=Ncuenta%>&secue='+ secuencia<%=i%>.value,'Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes  ');  x.focus();" >&nbsp
                                                                       -->
                                                                        </td>
                                                                <%}
                                                   }%>

							      <td class="bordereporte" align='center' nowrap> <input type='text' id='secuencia<%=i%>' name='secuencia<%=i%>'dir='rtl' readonly value='<%=  ( (anticipo.getSecuencia()==0)? "" : ""+anticipo.getSecuencia() ) %>' style="width:50px;font-size:10px;border:0" class='<%= (i%2==0?"filagris":"filaazul") %>'></td>


                                                              <!-- Julio Barros 17-11-2006 cambio de pocision-->
                                                              <td class="bordereporte" align='center' nowrap style="font-size:11px">
                                                              <% if( vista.equals("TRANSFERIR")   ){
															  		 %>
                                                                     <input type='checkbox' name='anticipo' value='<%= anticipo.getId()%>' id='GROUP<%=i%>' onClick=" cambiarColorMouse(fila<%=i%>); Sell_all_G(formulario,secuencia<%=i%>.value,GROUP<%=i%>.checked,<%=lista.size()%>);Totalizar_Select2(this.form,<%=lista.size()%>)">
                                                              <%}%>
                                                              </td>

                                                             <!-- Bancos -->
								<td class="bordereporte"  nowrap style="font-size:11px">
															      
								<%if((modelopav.ClientesVerService.ispermitted(perfil, "65")) || (usuario.getLogin().equals(anticipo.getUser_autorizacion()))){%>
								<img width='14' title='Asignar bancos' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/buscar.gif' 
								onclick= "javascript: var x= window.open( '<%= CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=LISTABANCOS&nit=<%=anticipo.getConductor()%>&anticipo=<%= anticipo.getId()%>&global=N&infoCTA=<%=Banco%>&Ncuenta=<%=Ncuenta%>&secue='+ secuencia<%=i%>.value,'Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes  ');  x.focus();" >  
								<%}%>
								&nbsp <%= anticipo.getBanco()%>
								</td>
                                                              <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%= anticipo.getSucursal()      %> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%= anticipo.getCuenta()        %> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%= anticipo.getTipo_cuenta()   %> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%= anticipo.getNombre_cuenta() %> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%= anticipo.getNit_cuenta()    %> </td>

                                                              <!-- Aprobador -->
                                                              <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%= anticipo.getFecha_autorizacion() %> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font-size:10px"> <%= anticipo.getUser_autorizacion()  %> </td>
															  <%if(anticipo.getSecuencia()== -1){%>
															  <td class="bordereporte" align='center' nowrap style="font-size:11px"><img width='14' title='Reversar la transaccion' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/anular.gif' onclick= "Confirm(formulario,secuencia<%=i%>.value); " > </td>
                                                         <%    }else{ %>
														 	  <td class="bordereporte" align='center' nowrap style="font-size:10px">&nbsp;  </td>
														 <%    }
														   }%>

                                               </tr><%--Conductor:<%=anticipo.getConductor()%>:id:<%= anticipo.getId()%> --%>
                                          <%}%>
                                   <%}%>

                                 </table>
                             </td>
                          </tr>

                   </table>
                 </td>
              </tr>
           </table>

          </form>
           <br>
           <% if( lista !=null && lista.size()>0  ){%>   <!-- rhonalf-->
           <img src="<%=BASEURL%>/images/botones/aceptar.gif"  id="aceptarimg"  height="21"  title='Aceptar' onclick='send(formulario)'                                                                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
           <!-- rhonalf -->
           <img alt="modificar giradora" src="<%=BASEURL%>/images/botones/modificar.gif" height="21"  title='Modificar Giradora' onclick='sendit()' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer">
           <!-- /rhonalf -->
           <% } %>

           <img src="<%=BASEURL%>/images/botones/restablecer.gif"   height="21"  title='Refrescar'  onClick="location.href='<%=CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=REFRESCAR&vista=<%=vista%>'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          <% if(  vista.equals("APROBAR") ){%>
           <img src="<%=BASEURL%>/images/botones/regresar.gif"      height="21"  title='Regresar'   onClick="location.href='<%=CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=INITCONSULTA'"                onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          <%}%>
           <img src="<%=BASEURL%>/images/botones/salir.gif"         height="21"  title='Salir'      onClick="window.close();"                                                                                          onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">



	<% if( lista !=null && lista.size()>0  && ( Ncuenta != null && Ncuenta.equals("FINTRA")) ){%>
		<br><a href="#" onClick="jscript: sendToPrestamo();" class="Simulacion_Hiper">Liquidar Prestamos Pendientes....</a>
	<% } %>

     <% if(msj!=null  &&  !msj.equals("") ){%>
                <BR><BR>
                <table border="2" align="center">
                      <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                  <tr>
                                        <td width="450" align="center" class="mensajes"><%= msj %></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp; </td>
                                  </tr>
                             </table>
                        </td>
                      </tr>
                </table>


     <%}%>

</div>
     
<div id="dialogo2" class="ventana">
       <p  id="msj2">texto </p> <br/>
       <center>
           <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
       </center>
</div>
<%=datos[1]%>
                <!--inicio de 20101014-->
		<%if (semirepetidos!=null && !(semirepetidos.equals("0"))){%>
		<script>
		    Dialog.closeInfo();
		    Dialog.alert('<br><center>Planillas aparentemente repetidas :&nbsp; <%=semirepetidos%> .</center>', {
				    width:300,
				    height:120
				});
		</script>
		<%}%>
		<!--fin de 20101014-->
</body>
</html>
