<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validarDocumentos.js"></script> 
</head>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Ingresar Anticipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Anticipos&accion=Insert&cmd=show" id="forma">
  <table width="530" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center">    
                <tr>                                    
                    <td nowrap width="50%" class='subtitulo1'>Datos</td>
                    <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>                                    
                </tr>
                <tr class="fila">
                  <td width="162" nowrap><strong>Codigo del Concepto </strong></td>
                  <td width="352" nowrap><input name="codant" type="text" class="textbox" id="codant" size="10" maxlength="6">          </td>
                </tr>
                <tr class="fila">
                  <td width="162" nowrap><strong>Descripcion</strong></td>
                  <td nowrap><input name="descant" type="text" class="textbox" id="descant" maxlength="30"></td>
                </tr>
                <tr class="fila">
                  <td nowrap><strong>Distrito</strong></td>
                  <td nowrap><select name="distrito" class="textbox" id="distrito">
                    <option value="FINV" selected>FINV</option>
                  </select></td>
                </tr>
                <tr class="fila">
                    <td nowrap><strong>Standard Job </strong></td>
                    <td nowrap>
                        <input name="sj" type="text" class="textbox" id="sj">	  
                    </td>
                </tr>
                <tr class="fila">
                  <td nowrap><strong>Tipo de Descuento</strong></td>
                  <td nowrap><select name="tipo_des" class="textbox" id="tipo_des">
                    <option value="V">Valor</option>
                    <option value="P">Porcentaje</option>
                  </select></td>
                </tr>
                <tr class="fila">
                  <td nowrap><strong>Valor</strong></td>
                  <td nowrap><input name="valor" type="text" class="textbox" id="valor"  onKeyPress="soloDigitos(event,'decOK')">
                    <select name="moneda" class="textbox" id="moneda">
                      <option value="PES">Pesos</option>
                      <option value="BOL">Bolivar</option>
                      <option value="DOL">Dolares</option>
                    </select></td>
                </tr>
                <tr class="fila">
                  <td nowrap><strong>Aplicar a </strong></td>
                  <td nowrap><select name="indicador" class="textbox" id="indicador">
                    <option value="S">Saldo</option>
                    <option value="V">Valor</option>
                  </select></td>
                </tr>
                <tr class="fila">
                  <td nowrap><strong>Codigo Migracion </strong></td>
                  <td nowrap><input name="migracion" type="text" class="textbox" id="migracion" size="2" maxlength="1"></td>
                </tr>
            </table>
        </td>
     </tr>
  </table>
  <br>
  <table align="center" width="530">
  	<tr>
		<td align="center">
			<img title='Ingresar anticipo' src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="return TCamposLlenos();">			
			<img title='Reiniciar valores' src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="forma.reset();"> </img>
			<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img>
		</td>
	</tr>
  </table>
</form>
</div>
</body>
</html>
