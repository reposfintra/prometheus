<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>


<html>
<head>
  <title>Clasificacion</title>
  <script>
  function validar(form){
            if (form.opcion.value=='llenarTabla'){
                if (form.id_usuario.value ==''){
                    alert('Defina el usuario para poder continuar...')
                    return false;
                }
            }
            return true;
      }
    function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
    }
	
	function ActAll(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    }
  </script>
  <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center class='comentario'>

<form name="form" method="post" action="<%=CONTROLLER%>?estado=Banco&accion=Usuario" onsubmit='jscript: return validarForm(this);'>
  <input type="hidden" name="opcion" value="llenarTabla">
    <table width="358" border="2" align="center">
    <tr>
      <td>
	  	<table width="100%" class="tablaInferior">
			<tr class="fila">
			  <td colspan="2" >
				  <table width="100%"  border="0" cellspacing="1" cellpadding="0">
					  <tr>
						<td width="50%" class="subtitulo1">&nbsp;Relacionar Bancos</td>
						<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					  </tr>
				  </table></td>
			</tr>
			<tr class="fila">
			  <td width="25%">Usuario:</td>
			  <td>
				<input name="id_usuario" type="text" id="id_usuario">      <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="20" height="20" style="cursor:hand" title="Buscar un usuario" name="buscar"  onClick="form.submit();" ></td>
		    </tr>
		  </table>
		</TD>
	 </tr>
	</table>
  </form>
  <% Vector bancos =  (Vector) session.getAttribute("bancos");
  	Usuario usuario = (Usuario) session.getAttribute("Usuario");

  	if ( bancos != null ){
   %>
  <form action="<%=CONTROLLER%>?estado=Banco&accion=Usuario" method='post' name='FormularioListado'>
  	<input type="hidden" name="opcion" value="guardarTabla">

        <br>
		<table width='620' align="center" border="2">
			<tr>
				<td>
					<table width="100%" align="center">
					  <tr>
						<td width="50%" class="subtitulo1">&nbsp;Lista Bancos Relacionados</td>
						<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					  </tr>
					</table>				
					<table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
					<tr class="tblTitulo">
						<th width='20' >N�</th>
						<th width='20' ><input type='checkbox' name='All' onclick='jscript: SelAll();'></th>
						<th width='50' >BANCO</th>
						<th width='330'>BRANCH CODE/BANK ACCOUNT </th>
					</tr>
					<%
						
						for( int i=0; i<bancos.size(); i++ ){
							Banco b = (Banco) bancos.elementAt(i);
							String Fondo = (i % 2 == 0 )?"filagris":"filaazul";
							String Estilo = (b.esDeUsuario())?"filaresaltada":Fondo;
					%>
					<tr class="<%=Estilo%>" >
						<td class="bordereporte" align='center'><span class='comentario'><%=(i+1)%></span></td>
						<% if ( b.esDeUsuario() ){ %>
							<th class="bordereporte"><input type='checkbox' name='CHK' value='<%= b.getDstrct() +"/"+ b.getBanco()+"/"+b.getBank_account_no() %>' checked onclick='jscript: ActAll();'></th>
						<% } else { %>
							<th class="bordereporte"><input type='checkbox' name='CHK' value='<%= b.getDstrct() +"/"+ b.getBanco()+"/"+b.getBank_account_no() %>' onclick='jscript: ActAll();'></th>
						<% } %>
						<td class="bordereporte" align='center'><%= b.getBanco() %></td>
						<td class="bordereporte"><%= b.getBanco() + "/" + b.getBank_account_no() %></td>
					</tr>
					<%  } 
					   String mensaje = request.getParameter("mensaje");
					   if ( mensaje == null ){
							mensaje = "";
					   }
					  %>        
				</table>
				</td>
		</tr>
	</table>
        <br>
		<p class="comentario"><%=mensaje%></p>
        	<p>
			<div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Actualizar" name="modificar"  onclick="FormularioListado.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
			</p>
  </form>
  <%  }
      else {
	  	if(request.getParameter("mensaje")!=null){
		   %> 
		  <table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")==null?"":request.getParameter("mensaje")%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
   	<% } 
   }%>
</center>
</body>
</html>
