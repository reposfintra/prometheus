<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
<link href="../css/letras.css" rel="stylesheet" type="text/css">

<title>Buscar Planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Search&cmd=show">
  <table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="Letras"><strong><strong>BUSQUE LA PLANILLA A ANULAR </strong></strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="162" nowrap><strong>Numero de la Planilla:</strong></td>
      <td width="352" nowrap bgcolor="#CEFFFF"><input name="planilla" type="text" id="planilla" maxlength="10">
      <input type="submit" name="Submit" value="Buscar"></td>
    </tr>
  </table>
</form>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Delete&cmd=show" onSubmit="return ValidarFormModificar(this);">
  <div align="center">
    <%String planilla="";
  	
	String placa="", trailer="", conductor="", nomcond="", estandard="", proveedorA="", proveedorAC="", proveedorT="", orden="", standar="", origen="", destino="" ;
	float pesolleno=0, pesov=0, ant=0, peajea=0, peajeb=0, peajec=0, galon=0;
 	if(request.getAttribute("planilla")!=null){
     	%>
		  <span class="letras" >�Esta seguro de Anular la siguiente Planilla?</span>		  <%Planilla pla= (Planilla)request.getAttribute("planilla");
		planilla=pla.getNumpla();
		placa=pla.getPlaveh();
		trailer=pla.getPlatlr();
		conductor= pla.getCedcon();
		nomcond=pla.getNomCond();
		orden=pla.getOrden_carga();
		estandard=pla.getSj_desc();
		standar= pla.getSj();
		origen = pla.getNomori();
        destino= pla.getNomdest();
	}
	if(request.getAttribute("plaaux")!=null){
		Plaaux plaaux= (Plaaux) request.getAttribute("plaaux");
		pesolleno = plaaux.getFull_weight();
		pesov=plaaux.getEmpty_weight();
		ant=plaaux.getAcpm_gln();
		peajea=plaaux.getTicket_a();
		peajeb=plaaux.getTicket_b();
		peajec=plaaux.getTicket_c();
		galon= plaaux.getAcpm_gln();
		proveedorAC=plaaux.getAcpm_supplier();
		proveedorT=plaaux.getTicket_supplier();
	}
	if(request.getAttribute("movpla")!=null){
		Movpla movpla= (Movpla)request.getAttribute("movpla");
		ant= movpla.getVlr();
		
	}
	%>
    <br>
	<br>  		
  </div>
  <table width="741" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFAA29">
      <td colspan="4"><div align="center"><span class="Letras"><strong>DATOS DE LA PLANILLA <%=planilla%>
              <input name="planilla" type="hidden" id="planilla" value="<%=planilla%>">
      </strong></span></div></td>
    </tr>
    <tr>
      <td width="165" bgcolor="#9CCFFF"><strong>Placa</strong></td>
      <td width="137"><span class="Letras"><%=placa%>
      </span></td>
      <td width="178" bgcolor="#9CCFFF"><span class="Letras"><strong>Estandar</strong></span></td>
      <td width="238"><%=estandard%>        <input name="standar" type="hidden" id="standar" value="<%=standar%>">      </td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Trailer</strong></td>
      <td><span class="Estilo6 "><span class="Letras"><%=trailer%></span>
      </span></td>
      <td bgcolor="#9CCFFF"><span class="Letras"><strong>Orden de carga </strong></span></td>
      <td><span class="Letras"><%=orden%></span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Conductor</strong></td>
      <td><span class="Estilo6 "><span class="Letras"><%=conductor%></span>
</span></td>
      <td bgcolor="#9CCFFF"><span class="Letras"><strong>Nombre Conductor </strong></span></td>
      <td><%=nomcond%></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Peso lleno </strong></td>
      <td><span class="Letras"><%=pesolleno%></span></td>
      <td bgcolor="#9CCFFF"><strong>Galon Acpm </strong></td>
      <td><span class="Estilo6"><%=galon%>
      </span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Peso vacio </strong></td>
      <td><span class="Estilo6 "><span class="Letras"><%=pesov%></span>
      </span></td>
      <td bgcolor="#9CCFFF"><span class="Letras"><strong>Proveedor de acpm </strong></span></td>
      <td><span class="Letras"><%=proveedorAC%></span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Valor Anticipo</strong></td>
      <td><span class="Letras"><%=ant%></span></td>
      <td bgcolor="#9CCFFF"><strong>Tiquete Peaje A </strong></td>
      <td><span class="Letras"><%=peajea%>
      </span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Proveedor de Tiquetes </strong></td>
      <td><span class="Estilo6"><span class="Letras"><%=proveedorT%></span>
</span></td>
      <td bgcolor="#9CCFFF"><strong>Tiquete Peaje B </strong></td>
      <td><span class="Letras"><%=peajeb%>
      </span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Origen </strong></td>
      <td><span class="Letras"><span class="Letras"><span class="Estilo6"><%=origen%></span></span>
      </span></td>
      <td bgcolor="#9CCFFF"><strong>Tiquete Peaje C</strong></td>
      <td> <span class="Letras"><%=peajec%>
      </span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Destino</strong></td>
      <td colspan="3"><span class="Estilo6"><span class="Letras"><%=destino%></span></span></td>
    </tr>
  </table>
  <br>

  <%if(request.getAttribute("planilla")!=null){%>
  <div align="center">
    <input type="submit" name="Submit" value="Anular">
    <input type="button" name="Submit2" value="Regresar">
    <br>
  </div>
  <%}%>
</form>
</body>
</html>
