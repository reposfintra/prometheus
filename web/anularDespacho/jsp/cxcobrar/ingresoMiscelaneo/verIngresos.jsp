<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 28 de noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, lista de ingresos

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Ingresos Miscelaneo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="file:///C|/Tomcat5/webapps/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

</head>
<body onLoad="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Ingresos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

  <%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	
	LinkedList lista = model.ingresoService.getListadoingreso();
    Ingreso ing;

	if ( lista.size() >0 ){  
%>
</p>
<table width="1050" border="2" align="center">
    <tr>
      <td width="100%">
	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Datos Ingresos Miscelaneo </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
          <td width="8%" height="46" align="center">Tipo</td>
          <td width="5%" align="center">Numero</td>
          <td width="14%"  align="center">Recibido Por</td>
          <td width="12%" align="center">Consignado</td>
          <td width="9%"  align="center">Fecha Consignacion </td>
          <td width="7%"  align="center"> Fecha Ingreso </td>
		  <td width="8%"  align="center">Banco </td>
		  <td width="10%"  align="center">Suscursal </td>
		  <td width="15%"  align="center">Cuenta </td>
		  <td width="12%"  align="center">Estado </td>
        </tr>
        <pg:pager
         items="<%=lista.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
         maxIndexPages="<%= maxIndexPages %>"
         isOffset="<%= true %>"
         export="offset,currentPageNumber=pageNumber"
         scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, lista.size()); i < l; i++)
	  {
          ing = (Ingreso) lista.get(i);
		  String vec[] = ing.getBank_account_no().split("/");
		  String suc = "", cuen = "";
			if( vec != null && vec.length > 1 ){
				suc = vec[0];
				cuen = vec[1];
			}
		  %>
        <pg:item>
        <tr class="<%=( ing.getReg_status().equals("A") )?"filaroja":(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
        onClick="window.open('<%=CONTROLLER%>?estado=IngresoMiscelaneo&accion=Buscar&evento=1&numero=<%=ing.getNum_ingreso()%>&dstrct=<%=ing.getDstrct()%>&tipodoc=<%=ing.getTipo_documento()%>&mostrar=ok' ,'M','status=yes,scrollbars=no,width=780,height=500,resizable=yes');">
          <td width="8%" class="bordereporte" nowrap><%=ing.getDestipo()%></td>
          <td width="5%" class="bordereporte" nowrap><%=ing.getNum_ingreso()%></td>
          <td width="14%" class="bordereporte" align="center" nowrap><%=ing.getNomCliente()%></td> 
          <td width="12%" class="bordereporte" align="right" nowrap><%=(ing.getCodmoneda().equals("DOL"))?UtilFinanzas.customFormat2( ing.getVlr_ingreso_me() ):UtilFinanzas.customFormat(ing.getVlr_ingreso_me())%> <%=ing.getCodmoneda()%></td>
          <td width="9%" class="bordereporte" align="center" nowrap><%=ing.getFecha_consignacion()%></td>
          <td width="7%" class="bordereporte" align="center" nowrap><%=ing.getFecha_ingreso()%></td>
		  <td width="8%" class="bordereporte" align="center" nowrap><%=!ing.getBranch_code().equals("")?ing.getBranch_code():"&nbsp;"%></td>
		  <td width="10%" class="bordereporte" align="center" nowrap><%=!suc.equals("")?suc:"&nbsp;"%></td>
		  <td width="15%" class="bordereporte" align="center" nowrap><%=!cuen.equals("")?cuen:"&nbsp;"%></td>
		  <td  class="bordereporte" align="center" nowrap><%=ing.getReg_status().equals("A")?"ANULADO":"ACTIVO"%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="bordereporte">
          <td td height="20" colspan="11" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
  <p>
      <%}
 else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
  <%}%>
<table width="1050" border="0" align="center">
   <tr>
     <td width="900"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=BuscarIngreso.jsp&carpeta=/jsp/cxcobrar/ingresoMiscelaneo&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
 </table>
</div> 
</body>

</html>
