<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Agregar Anticipos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validarDespacho.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">



</head>

<body onLoad="<%if(request.getParameter("cerrar")!=null){%>window.close();<%}%>">
<form action="<%=CONTROLLER%>?estado=Anticipos&accion=Aplicar&cmd=show" method="post" name="form2" id="form2">
 <table width="70%"  border="2" align="center" cellpadding="0" cellspacing="0">
	<tr>
	  <td>
  <table width="100%" class="tablaInferior">
    <tr>
      <td colspan=2 class="subtitulo1">Anticipos<span class="Letras"></span></td>
      <td width="62%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" cellpadding="3" cellspacing="2">
  <%
  String tipo = request.getParameter("tipo")==null?"1":request.getParameter("tipo");
  Vector descuentos = model.anticiposService.getAnticiposProv();
  if(descuentos!=null){
	  for(int k = 0 ; k<descuentos.size();k++){
				Anticipos ant = (Anticipos) descuentos.elementAt(k); 
				String codigo = ant.getAnticipo_code();
				float valor = ant.getValor();
				//model.proveedoresService.listaAnticipoProvee(codigo);
				Vector provee = ant.getProveedores();
				
				
			%>
			
  
	<tr >
	    <td width="31%" height="24" class="filaresaltada"><%=ant.getAnticipo_desc().toUpperCase()%></td>
        <td width="25%" class="letra">
		<input name="<%=ant.getAnticipo_code()%>" type="text" onKeyPress="soloDigitos(event,'decNo')" class="textbox"  value="<%=valor==0?"":String.valueOf(valor)%>" size="18" <%=ant.isChequeado()?"readOnly":""%>>
		<%=ant.isChequeado()?"Impreso":""%>			
		</td>
        
		<td width="44%" class="letra">
		<%if(provee.size()>0 && !ant.isChequeado()){%>
		<select name="provee<%=codigo%>" class="textbox">
                <%for(int m =0; m<provee.size(); m++){
				Proveedores prov = (Proveedores)provee.elementAt(m);
			%>
                <option value="<%=prov.getNit()%>/<%=prov.getSucursal()%>" <%=ant.getProveedor().equals(prov.getNit()+"/"+prov.getSucursal())?"selected":""%>><%=prov.getNombre()%> <%=prov.getSucursal()%></option>
                <%}%>
            </select><%}
			else{%>
				<%=ant.getProveedor()%><input name="provee<%=codigo%>" type="hidden" value="<%=ant.getProveedor()%>">
			<%}%>
</td>
	</tr>
	<%}
	}%>
  </table>
	  </td>
  </tr></table>
 <div align="center"><br>
<%if(descuentos!=null){
	if(descuentos.size()>0){%>
   <img name="imageField" src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="aplicarAnticipos('<%=CONTROLLER%>','<%=tipo%>','<%=request.getParameter("remesa")%>');" >
  <%}
  }%>
   <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
 </div>
</form>
</body>
</html>
