# README
# Fecha de creacion 2018-10-23 16:10

### Instructivo de configuracion db.properties, sitio web fintra, selectrik y provintegral 

Primero se debe crear un archivo en el package com.tsp.util.connectionpool, llamado db.properties.   
Una vez creado debes agregar las siguientes configuraciones para definir las conexiones a la base de datos.   
Debes reemplazar  {carpeta de proyectos} ejemplo  /root/sitios/fintra/,  por la ruta local de tu carpeta de proyectos.   

>>>
drivers=org.postgresql.Driver  
ruta={carpeta de proyectos}/build/web  
equipo=localhost 

KF=70   
KI=30   

fintra.url=jdbc\:postgresql\:// ip o dominio base datos\:5432/fintra  
fintra.user=postgres  
fintra.password=*******  
fintra.maxconns=200  

selectrik.url=jdbc\:postgresql\:// ip o dominio base datos\:5432/selectrik  
selectrik.user=postgres  
selectrik.password=*******  
selectrik.maxconns=10  

cobranza.url=jdbc\:postgresql\:// ip o dominio base datos\:5432/cobranza  
cobranza.user=postgres  
cobranza.password=*******  
cobranza.maxconns=10  

provintegral.url=jdbc\:postgresql\:// ip o dominio base datos\:5432/provintegral  
provintegral.user=postgres  
provintegral.password=*******  
provintegral.maxconns=5  

>>>
# Configuracion de rutas relativas 


### Esto aplicado para pathXmlDao, opav.pathXmlDao, logfile, rutaInformes, rutaLicenciaAspose.

>>>
connpooldatasrc=connpoolds  
pathXmlDao=file\:{carpeta de proyectos}/build/web/WEB-INF/classes/com/tsp/operation/model/DAOS/xml/  
opav.pathXmlDao=file\:{carpeta de proyectos}/build/web/WEB-INF/classes/com/tsp/opav/model/DAOS/xml/  

logfile={carpeta de proyectos}/build/web/logs/poolmanager.log  
rutaInformes={carpeta de proyectos}/build/web/reportes  
rutaLicenciaAspose={carpeta de proyectos}/build/web/WEB-INF/classes/com/aspose/cells/Aspose.Total.Java.lic  

url=http://localhost:8094/fintra/ContabilizacionServlet  
url_correos_anticipos=http://localhost:8094/fintra/CorreosAnticiposServlet  
causacion_interes_mc=http://localhost:8094/fintra/CausacionInteresesMCServlet?caso=1   
causacion_interes_mc_fmes=http://localhost:8094/fintra/CausacionInteresesMCServlet?caso=2  
causacion_interes_mciclo=http://localhost:8094/fintra/CausacionInteresesMCServlet?caso=3  
facturas_cat=http://localhost:8094/fintra/FacturasCATServlet  
descarga_tit_indem=http://localhost:8080/fintra/DescargaTitulosIndemServlet  
cuota_manejo=http://localhost:8094/fintra/CausacionCuotaManejoServlet  
>>>

# Configuracion de local storage Ruta de imagenes
>>>
rutaImagenes=/imagenesfintra/

>>>